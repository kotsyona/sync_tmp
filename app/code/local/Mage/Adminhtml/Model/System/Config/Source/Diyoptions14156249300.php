<?php
class Mage_Adminhtml_Model_System_Config_Source_Diyoptions14156249300
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(

            array('value' => 1, 'label'=>Mage::helper('adminhtml')->__('manual mode')),
            array('value' => 2, 'label'=>Mage::helper('adminhtml')->__('machine mode')),
        );
    }

}
