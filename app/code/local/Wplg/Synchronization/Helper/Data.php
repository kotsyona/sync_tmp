<?php

class Wplg_Synchronization_Helper_Data extends Mage_Core_Helper_Abstract {


    public function getAdapterName() {
        return "adapter" . str_replace(array('www.', '.', '-'), array('', '', ''), $_SERVER['SERVER_NAME']);
    }

    public function saveLog($fileName, $content, $continue = false, $title = '') {
        $oldContent = '';
        if ($continue) {
            $oldContent = file_get_contents($fileName);
        }
        file_put_contents($fileName, $oldContent . $title . ' : ' .$content . PHP_EOL);
    }

    /**
     * Получить список категорий и сформировать массив
     * @return type array
     */
    public function getListCategory() {
        $list = array();
        $categories = Mage::getModel('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->load();
        foreach ($categories as $listCat) {
            if($listCat->getId()>1){
                $list[] = array (   'id'    => $listCat->getId(),
                    'pid'   => $listCat->getParentId(),
                    'name'  => $listCat->getName()
                );
            }
        }
        return $list;
    }

    public function getNamesOfColumnsInFileFromConfig() {
        $namesOfColumnsString = (string)Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/columns_name', Mage::app()->getStore());
        $namesOfColumns = explode(',', $namesOfColumnsString);

        return $namesOfColumns;
    }

    public function getAttributeInfoFromCsvHeaderCell($headerCell) {
        $processingMode = (string) Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/processing_mode', Mage::app()->getStore());
        $attribute = array();

        $attrArray = explode("|", $headerCell);
        $attributeCode = Mage::helper('synchronization')->cyr2lat(trim($attrArray[0]));
        $attribute['label'] = trim($attrArray[0]);
        $attribute['code'] = $attributeCode;
        $attribute['type'] = trim($attrArray[1]);
        $attribute['key'] = ($processingMode == 1) ? $attributeCode : trim($attrArray[2]);

        return $attribute;
    }

    /**
     * Generates a path, where the images to import
     * @return type
     */
    public function getUrlPathForImage($folderName) {
        return trim(Mage::getBaseDir('media'), '/') . DS . 'synchronization' . DS . 'images' . DS . $folderName;
    }

    /***
     * Get images by sku
     */
    public function getFilesImgBySku($path, $sku) {

        $mask = DS . $path . DS . '*{' . $sku . '_}*.jpg';
        $imagesList = glob($mask, GLOB_BRACE);

        $mainImage = DS . $path . DS . $sku . '.jpg';
        if (file_exists($mainImage)) {
            $imagesList['main'] = $mainImage;
        }

        return $imagesList;
    }

    /***
     * Get images name from csv
     */
    public function getFilesImgFromCsv($path, $imgNames) {
        $fileNames = NULL;

        foreach (explode(",", $imgNames) as $key => $name) {

            /***  */
            $imageFile = DS . $path . DS . trim($name);
            if (file_exists($imageFile)) {
                if($key === 0) {
                    $fileNames["main"] = $imageFile;
                } else {
                    $arrayKey = "z". $key;
                    $fileNames[$arrayKey] = $imageFile;
                }
                /*** unset($fileNames[$arrayKey]); */
            }

            /***if($key === 0) {
                unset($fileNames[0]);
            }*/

            ksort($fileNames);
        }
        return $fileNames;
    }

    /*
     * Get header from CSV file
     */
    public function getHeaderFromCsv($filename = '') {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        try {
            $csv = new SplFileObject($filename, 'r');
        } catch (RuntimeException $e) {
            return FALSE;
        }

        $header = $csv->fgetcsv();

        return $header;
    }

    public function csv_to_array($filename = '', $delimiter = ',', $csvKeys) {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $count = 0;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                $count++;
                if ($count == 1) { continue; }
                $data[] = array_combine($csvKeys, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    /*
     * Get some lines from csv file
     */
    public function some_csv_lines_to_array($position, $parseAtOnce, $filename = '', $csvKeys = '') {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        try {
            $csv = new SplFileObject($filename, 'r');
        } catch (RuntimeException $e) {
            return FALSE;
        }

        $endPosition = $position + $parseAtOnce;
        $all_rows = array();

        while (($position < $endPosition) && ($csv->fgetcsv())) {

            if ($position == 0) {
                $position++;
            }

            $csv->seek($position);
            $currentString = str_replace("\xc2\xa0", " ", trim($csv->current()));
            $row = str_getcsv($currentString);

            $all_rows[] = (empty($csvKeys)) ? $row : array_combine($csvKeys, $row);
            $position++;
        }

        $nextPosition = ($csv->eof()) ? 'end' : $csv->key();

        return array($all_rows, $nextPosition);
    }

    public function getSyncPath($subPath = NULL) {

        if($subPath){
            $path = Mage::getBaseDir() . DS . 'media' . DS . 'synchronization' . DS . $subPath . DS;
        }else{
            $path = Mage::getBaseDir() . DS . 'media' . DS . 'synchronization' . DS;
        }

        return $path;
    }

    public function getSyncDirUrl($subDir = NULL) {

        if($subDir){
            $url = Mage::getBaseUrl('media') . 'synchronization' . DS . $subDir . DS;
        }else{
            $url = Mage::getBaseUrl('media') . 'synchronization' . DS;
        }

        return $url;
    }

    /**
     * Get all files in $path
     * @param type $path
     * @return type array
     */
    public function getListCSVFileToParse($path) {
        $file = array();
        foreach (glob($path . "*.csv") as $filename) {
            $file[] = str_replace($path, '', $filename);
        }
        return $file;
    }

    public function getCSVFileToParse($path) {
        $file = FALSE;
        foreach (glob($path . "*.csv") as $filename) {
            $file = str_replace($path, '', $filename);
        }

        return $file;
    }


     /**
     *  Преобразование кирилицы в латинницу
     */
    public function cyr2lat($string) {
        $convertTable = array(
            '&amp;' => 'and', '@' => 'at', '©' => 'c', '®' => 'r', 'À' => 'a',
            'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'Å' => 'a', 'Æ' => 'ae', 'Ç' => 'c',
            'È' => 'e', 'É' => 'e', 'Ë' => 'e', 'Ì' => 'i', 'Í' => 'i', 'Î' => 'i',
            'Ï' => 'i', 'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Õ' => 'o', 'Ö' => 'o',
            'Ø' => 'o', 'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'Ý' => 'y',
            'ß' => 'ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', 'å' => 'a',
            'æ' => 'ae', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e',
            'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u',
            'û' => 'u', 'ü' => 'u', 'ý' => 'y', 'þ' => 'p', 'ÿ' => 'y', 'Ā' => 'a',
            'ā' => 'a', 'Ă' => 'a', 'ă' => 'a', 'Ą' => 'a', 'ą' => 'a', 'Ć' => 'c',
            'ć' => 'c', 'Ĉ' => 'c', 'ĉ' => 'c', 'Ċ' => 'c', 'ċ' => 'c', 'Č' => 'c',
            'č' => 'c', 'Ď' => 'd', 'ď' => 'd', 'Đ' => 'd', 'đ' => 'd', 'Ē' => 'e',
            'ē' => 'e', 'Ĕ' => 'e', 'ĕ' => 'e', 'Ė' => 'e', 'ė' => 'e', 'Ę' => 'e',
            'ę' => 'e', 'Ě' => 'e', 'ě' => 'e', 'Ĝ' => 'g', 'ĝ' => 'g', 'Ğ' => 'g',
            'ğ' => 'g', 'Ġ' => 'g', 'ġ' => 'g', 'Ģ' => 'g', 'ģ' => 'g', 'Ĥ' => 'h',
            'ĥ' => 'h', 'Ħ' => 'h', 'ħ' => 'h', 'Ĩ' => 'i', 'ĩ' => 'i', 'Ī' => 'i',
            'ī' => 'i', 'Ĭ' => 'i', 'ĭ' => 'i', 'Į' => 'i', 'į' => 'i', 'İ' => 'i',
            'ı' => 'i', 'Ĳ' => 'ij', 'ĳ' => 'ij', 'Ĵ' => 'j', 'ĵ' => 'j', 'Ķ' => 'k',
            'ķ' => 'k', 'ĸ' => 'k', 'Ĺ' => 'l', 'ĺ' => 'l', 'Ļ' => 'l', 'ļ' => 'l',
            'Ľ' => 'l', 'ľ' => 'l', 'Ŀ' => 'l', 'ŀ' => 'l', 'Ł' => 'l', 'ł' => 'l',
            'Ń' => 'n', 'ń' => 'n', 'Ņ' => 'n', 'ņ' => 'n', 'Ň' => 'n', 'ň' => 'n',
            'ŉ' => 'n', 'Ŋ' => 'n', 'ŋ' => 'n', 'Ō' => 'o', 'ō' => 'o', 'Ŏ' => 'o',
            'ŏ' => 'o', 'Ő' => 'o', 'ő' => 'o', 'Œ' => 'oe', 'œ' => 'oe', 'Ŕ' => 'r',
            'ŕ' => 'r', 'Ŗ' => 'r', 'ŗ' => 'r', 'Ř' => 'r', 'ř' => 'r', 'Ś' => 's',
            'ś' => 's', 'Ŝ' => 's', 'ŝ' => 's', 'Ş' => 's', 'ş' => 's', 'Š' => 's',
            'š' => 's', 'Ţ' => 't', 'ţ' => 't', 'Ť' => 't', 'ť' => 't', 'Ŧ' => 't',
            'ŧ' => 't', 'Ũ' => 'u', 'ũ' => 'u', 'Ū' => 'u', 'ū' => 'u', 'Ŭ' => 'u',
            'ŭ' => 'u', 'Ů' => 'u', 'ů' => 'u', 'Ű' => 'u', 'ű' => 'u', 'Ų' => 'u',
            'ų' => 'u', 'Ŵ' => 'w', 'ŵ' => 'w', 'Ŷ' => 'y', 'ŷ' => 'y', 'Ÿ' => 'y',
            'Ź' => 'z', 'ź' => 'z', 'Ż' => 'z', 'ż' => 'z', 'Ž' => 'z', 'ž' => 'z',
            'ſ' => 'z', 'Ə' => 'e', 'ƒ' => 'f', 'Ơ' => 'o', 'ơ' => 'o', 'Ư' => 'u',
            'ư' => 'u', 'Ǎ' => 'a', 'ǎ' => 'a', 'Ǐ' => 'i', 'ǐ' => 'i', 'Ǒ' => 'o',
            'ǒ' => 'o', 'Ǔ' => 'u', 'ǔ' => 'u', 'Ǖ' => 'u', 'ǖ' => 'u', 'Ǘ' => 'u',
            'ǘ' => 'u', 'Ǚ' => 'u', 'ǚ' => 'u', 'Ǜ' => 'u', 'ǜ' => 'u', 'Ǻ' => 'a',
            'ǻ' => 'a', 'Ǽ' => 'ae', 'ǽ' => 'ae', 'Ǿ' => 'o', 'ǿ' => 'o', 'ə' => 'e',
            'Ё' => 'jo', 'Є' => 'e', 'І' => 'i', 'Ї' => 'i', 'А' => 'a', 'Б' => 'b',
            'В' => 'v', 'Г' => 'g', 'Д' => 'd', 'Е' => 'e', 'Ж' => 'zh', 'З' => 'z',
            'И' => 'i', 'Й' => 'y', 'К' => 'k', 'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r', 'С' => 's', 'Т' => 't', 'У' => 'u',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c', 'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'sch',
            'Ъ' => '', 'Ы' => 'y', 'Ь' => '', 'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l',
            'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's',
            'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
            'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e',
            'ю' => 'ju', 'я' => 'ya', 'ё' => 'jo', 'є' => 'e', 'і' => 'i', 'ї' => 'i',
            'Ґ' => 'g', 'ґ' => 'g', 'א' => 'a', 'ב' => 'b', 'ג' => 'g', 'ד' => 'd',
            'ה' => 'h', 'ו' => 'v', 'ז' => 'z', 'ח' => 'h', 'ט' => 't', 'י' => 'i',
            'ך' => 'k', 'כ' => 'k', 'ל' => 'l', 'ם' => 'm', 'מ' => 'm', 'ן' => 'n',
            'נ' => 'n', 'ס' => 's', 'ע' => 'e', 'ף' => 'p', 'פ' => 'p', 'ץ' => 'C',
            'צ' => 'c', 'ק' => 'q', 'ר' => 'r', 'ש' => 'w', 'ת' => 't', '™' => 'tm',
        );

        $string = strtr($string, $convertTable);

        $string = preg_replace('#[^0-9a-z]+#i', '_', $string);
        $string = strtolower($string);
        $string = trim($string, '-');
        return $string;
    }

    public function getListOfFilesInDirectory($dir) {
        $result = false;

        if (is_dir($dir)){
            $files = preg_grep('/^([^.])/', scandir($dir));
            foreach ($files as $file) {
                $result[] = $file;
            }
        }
        return $result;
    }

    public function deleteAllFilesFromPath($path) {
        try {
            foreach (glob($path . "*.*") as $file) {
                unlink($file);
            }
        } catch (Exception $ex) {
            echo $ex;
            return;
        }
    }

}
