<?php

class Wplg_Synchronization_Model_Filestoimport extends Mage_Core_Model_Abstract {

    protected $_optionParams = array(); /* file option parameters*/
    protected $_helper;
    protected $_messages = array();

    protected function _construct(){
       $this->_init("synchronization/filestoimport");
       $this->_helper = Mage::helper('synchronization');
    }


    /**
     * Parse header from csv file
     */
    public function parseHeaderCsv($path, $fname) {
        $result = false;
        $headerFromCsv = $this->_helper->getHeaderFromCsv($path . $fname);
        $headerFromConfig = $this->_helper->getNamesOfColumnsInFileFromConfig();

        if ($this->_checkMainColumns($headerFromCsv, $headerFromConfig)) {
            try {
                $fileToImportData = array(
                    'position' => '0',
                    'filename' => $fname,
                    'total_rows' => count(file($path . $fname)),
                    'attribute_codes' => $this->_getAttributeCodesFromCSVHeader($headerFromCsv, count($headerFromConfig))
                    );
                Mage::getModel("synchronization/filestoimport")->setData($fileToImportData)->save();
                $this->_messages[] = Mage::helper('synchronization')->__('Your file has been uploaded successfully.') . ' - ' . $fname;
                $result = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        } else {
            unlink($path . $fname);
        }

        Mage::getSingleton('core/session')->setMessageImportProducts(serialize($this->_messages));
        return $result;
    }

    protected function _checkMainColumns($headerFromCsv, $headerFromConfig) {
        $result = false;
        for ($i = 0; $i < count($headerFromConfig); $i++) {
            if ($headerFromCsv[$i] !== $headerFromConfig[$i]) {
                $this->_messages[] = 'Название колонки "' . $headerFromCsv[$i] . '" из Csv файла не соответсвует "' . $headerFromConfig[$i] . '" указаном в конфигурации';
            }
        }
        if (count($this->_messages) == 0) {
            $result = true;
        }

        return $result;
    }

    protected function _getAttributeCodesFromCSVHeader($headerFromCsv, $positionAttributes) {
        $associationModel = Mage::getModel("synchronization/association");
        $attributesToSave = array();
        for ($i = $positionAttributes, $count = count($headerFromCsv); $i < $count; $i++) {
            $attribute = $this->_helper->getAttributeInfoFromCsvHeaderCell($headerFromCsv[$i]);
            if ($associationModel->checkIfExistInAssociationTable('attribute', $attribute['key']) === false) {
                $attributeId = (int)Mage::getModel("synchronization/attribute")->checkOrAddAttributeToMagento($attribute['label'], $attribute['code'], $attribute['type']);
                $associationModel->addToAssociationTable('attribute', $attribute['key'], $attributeId);
                $associationModel->addToAssociationTable('attribute_type', $attribute['key'], $attribute['type']);
            }
            $attributesToSave[$i] = $attribute['key'];
        }

        return json_encode($attributesToSave);
    }

    /**
     * Get all files to import
     *
     * @return array
     */
    public function getFilesToImport() {
        $collection = $this->getCollection();

        return $collection->getData();
    }

    /**
     * Get file to import
     *
     * @return array
     */
    public function getFileToImport() {
        $collection = $this->getCollection()
                ->setPageSize(1);

        return $collection->getFirstItem();
    }

    /**
     * Check if exist files to import
     *
     * @return json array
     */
    public function processStatus() {
        $this->_normalize();
        $file = $this->getFileToImport();

        if ($file->getId() !== NULL) {
            $id = $file->getId();
            $position = $file->getPosition();
            $filename = $file->getFilename();
            $totalRows = $file->getTotalRows();
            $status = $file->getStatus();
            $options = $file->getOptions();

            $result = array(
                'progressStatus' => 'continue',
                'id' => $id,
                'filename' => $filename,
                'position' => $position,
                'total_rows' => $totalRows,
                'status' => $status,
                'options' => $options
            );
        } else {
            $result = array(
                'progressStatus' => 'end',
                'id' => '',
                'filename' => Mage::helper('synchronization')->__('Processing is complete'),
                'position' => 1,
                'total_rows' => 1,
                'status' => ''
            );

            /* Clean association table by type "configurable_simple" */
            Mage::getModel("synchronization/association")->cleanGarbage('configurable_simple');
            /* END*/
        }
        return json_encode($result);
    }

    /**
     * Normalize after each file processed
     *
     */
    protected function _normalize() {
        $files = $this->getCollection()
                ->addFieldToSelect('*')
                ->getData();
        $path = Mage::helper('synchronization')->getSyncPath('files');
        try {
            foreach ($files as $file) {
                $fileId = $file['id'];
                $filename = $file['filename'];
                $position = $file['position'];
                $totalRows = $file['total_rows'];
                if ($position == $totalRows) {

                    $modelAdapter = Mage::getModel("synchronization/adapter");
                    $modelAdapter->fixedCallBackGroupProductParam($fileId);
                    $modelAdapter->fixedCallBackSimpleProductParam();

                    unlink($path . $filename);
                    $this->load($fileId)->delete();
                    $this->reset();
                }
            }
            return TRUE;
        } catch (Exception $exc) {
            return FALSE;
        }
    }

    public function setOptionParamsByKey($key, $value) {
        $this->_optionParams[$key] = $value;
    }

    public function getOptionParamsByKey($key) {
        return $this->_optionParams[$key];
    }

    public function setOptionParams($optionParams) {
        $this->_optionParams = $optionParams;
    }

    public function getAllOptionParams() {
        return $this->_optionParams;
    }

    public function getCountOptionParams() {
        return count($this->_optionParams);
    }

    public function clearOptionParamsByKey($key) {
        unset($this->_optionParams[$key]);
    }

}
