<?php
class Wplg_Synchronization_Model_Attributeset extends Mage_Core_Model_Abstract {

    public function toOptionArray() {

        $atributeSetArray = array();

        $atributeSetArray[0] = array(
            'label' => Mage::helper('synchronization')->__('-- Please Select the Attribute set --'),
            'value' => 0
        );

        $attributeSets = Mage::getModel('catalog/product_attribute_set_api')->items();

        if (count($attributeSets) > 0) {
            foreach ($attributeSets as $attributeSet) {
                $atributeSetArray[] = array(
                    'label' => $attributeSet['name'],
                    'value' => $attributeSet['set_id'],
                );
            }
        }

        return $atributeSetArray;
    }

}