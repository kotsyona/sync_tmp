<?php

class Wplg_Synchronization_Model_Adaptermirotofbagscom extends Mage_Core_Model_Abstract {

    protected $_productData = array();
    /*Код категории	Тип продукта    Артикул    Имя       Цена   Количество    Папка с изображениями   Изображения   Цвет   Тип столешницы*/
    /*protected $_csvKeys = array('kod_kategorii', 'product_type', 'sku', 'name', 'price', 'qty', 'folder_name', 'image_names', 'cvet', 'tip_stoleshnicy');*/
    protected $_csvKeys;
    protected $_modelFilesToImport;
    protected $_modelProduct;
    protected $_modelAttributeOptions;
    protected $_helper;
    protected $attribute_set_ids = array(4);

    protected function _construct() {
        $this->_setProductData();
        $this->_modelFilesToImport = Mage::getModel("synchronization/filestoimport");
        $this->_modelProduct = Mage::getModel("synchronization/product");
        $this->_modelAttributeOptions = Mage::getModel("synchronization/attroptions");
        $this->_helper = Mage::helper('synchronization');
        $this->_csvKeys = $this->_helper->getNamesOfColumnsInFileFromConfig();
    }

    protected function _setProductData() {
        $this->_productData = array(
          'attribute_set_id' => 4,
          'weight' => 1,
          'price' => 0,
          'status' => 1,
          'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
          'is_active' => 1,
          'type_id' => 'simple',
          'tax_class_id' => '0',
          'category_ids' => 2, /* ToDo*/
          'website_ids' => 1,
        );
    }

    public function getProductData() {
        return $this->_productData;
    }

    public function parseFile() {
        $result = false;
        $file = $this->_modelFilesToImport->getFileToImport();

        $fileId = $file->getId();
        $position = $file->getPosition();
        $filename = $file->getFilename();
        $totalRows = $file->getTotalRows();
        $attributeCodes = (array)json_decode($file->getAttributeCodes());

        $parseAtOnceFromConfig = Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/parseatonce', Mage::app()->getStore());
        $parseAtOnce = empty($parseAtOnceFromConfig) ? 1 : $parseAtOnceFromConfig;

        $endPosition = $position + $parseAtOnce;

        while ($position < $endPosition && $position != $totalRows && !empty($filename)) {

            $parseResult = $this->_helper->some_csv_lines_to_array($position, $parseAtOnce, $this->_helper->getSyncPath('files') . $filename);

            $parseData = $parseResult[0];
            $nextPosition = $parseResult[1];

            foreach ($parseData as $data) {
                /* get magento category id */
                $categoryId = Mage::getModel("synchronization/categorysynchronization")->getMagentoIdCategoryByLocalIdCategory(trim($data[0]));
                if(!empty($categoryId)) {
                    $data[0] = $categoryId[0]['id_site_category'];
                } else {
                    break;
                }

                /* get magento product type id */
                $productTypeId = Mage::getModel("synchronization/producttypesynchronization")->getMagentoIdProductTypeByLocalIdProductType(trim($data[1]));
                if(!empty($productTypeId)) {
                    $data[1] = $productTypeId[0]['id_site_product_type'];
                } else {
                    break;
                }

                /* product attribute options */
                $attributeValuesArray = array();
                foreach ($attributeCodes as $attributePosition => $attributeKeyInAssociation) {
                    $attributeValues = trim($data[$attributePosition]);
                    if (strlen($attributeValues) > 0) {
                        $attributeCode = $this->_modelAttributeOptions->getAttributeCodeByKeyInAssociation($attributeKeyInAssociation);
                        $attributeValuesArray[$attributeCode] = $attributeValues;
                    }
                }

                /* array productData */
                $productData = $this->setProduct($data);
                if ($productData) {
                    $productId = $this->_modelProduct->saveProduct($productData, trim($data[5]));
                    /* $this->_modelProduct->saveProductAttributesOptionsByOptionIds($productId, $attributesToSave); */
                    $this->_modelAttributeOptions->saveProductAttributeOptions($productId, $attributeCodes, $attributeValuesArray);
                    /* save images to product */
                    if (strlen(trim($data[6])) > 0) {
                        if (strlen(trim($data[7])) > 0) {
                            $up_img = $this->_helper->getFilesImgFromCsv($this->_helper->getUrlPathForImage(trim($data[6])), trim($data[7]));
                            $this->_modelProduct->saveProductImages($up_img, $productId, $productData['sku']);
                        }
                    }
                }
            }

            $position = ($nextPosition == 'end') ? $position = $totalRows : (int) $nextPosition + 1;
            $processData = array('id' => $fileId, 'filename' => $filename, 'position' => $position);

            try {
                $this->_modelFilesToImport->setData($processData)->save();
                $result = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        return $result;
    }

    public function fixedCallBackGroupProductParam($fileId) {
    }

    public function fixedCallBackSimpleProductParam() {
    }

    public function setProduct($data) {
        /*** array('kod_kategorii', 'product_type', 'sku', 'name', 'price', 'qty', 'folder_name', 'image_names', 'cvet', 'tip_stoleshnicy') */

        if ((strlen(trim($data[2])) < 2)) {
            return false;
        }
        $productData = $this->getProductData();

        $productData['attribute_set_id'] = $data[1];
        $productData['category_ids'] = $data[0];

        $productData['name'] = trim($data[3]); /* TODO check if empty*/
        $productData['sku'] = trim($data[2]);
        $productData['price'] = trim($data[4]);
        $productData['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;


        return $productData;
    }

}
