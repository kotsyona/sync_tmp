<?php

class Wplg_Synchronization_Model_Adapter extends Mage_Core_Model_Abstract {

    protected $_productData = array();
    /*Бренд	Груп. Товар	Артикул	Наименование		Мл	Вид	Пол	Цена*/
    protected $_csvKeys = array('brand', 'grouped', 'sku', 'name', 'grouped_sku', 'mililitru', 'vud', 'pol', 'price');
    protected $_modelFilesToImport;
    protected $_modelProduct;
    protected $_helper;

    protected function _construct() {
        $this->_setProductData();
        $this->_modelFilesToImport = Mage::getModel("synchronization/filestoimport");
        $this->_modelProduct = Mage::getModel("synchronization/product");
        $this->_helper = Mage::helper('synchronization');
    }

    protected function _setProductData() {
        $this->_productData = array(
            'attribute_set_id' => 4,
            'weight' => 1,
            'price' => 0,
            'status' => 1,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'is_active' => 1,
            'type_id' => 'simple',
            'tax_class_id' => '0',
            'category_ids' => 2, /* ToDo*/
            'web_site_ids' => array(Mage::app()->getStore(true)->getWebsite()->getId()),
            'store_id' => Mage::app()->getStore()->getStoreId(),
            'is_in_stock' => 1,
            'qty' => 1000, /* ToDo*/
        );
    }

    public function getProductData($param) {
        return $this->_productData;
    }

    public function parseFile() {
        /*** echo '{"id":"1","filename":"2014-10-03_17-10-37_original-parphum.csv","position":"40","total_rows":"61","status":"0","options":null} '; */
        $result = false;
        $file = $this->_modelFilesToImport->getFileToImport();

        $id = $file->getId();
        $position = $file->getPosition();
        $filename = $file->getFilename();
        $totalRows = $file->getTotalRows();

        /* initial*/
        $this->_modelFilesToImport->setOptionParams(json_decode($this->_modelFilesToImport->load($id)->getOptions(), true));

        $parseAtOnceFromConfig = Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/parseatonce', Mage::app()->getStore());
        $parseAtOnce = empty($parseAtOnceFromConfig) ? 1 : $parseAtOnceFromConfig;

        $endPosition = $position + $parseAtOnce;

        while ($position < $endPosition && $position != $totalRows && !empty($filename)) {

            $parseData = $this->_helper->some_csv_lines_to_array($position, $parseAtOnce, $this->_helper->getSyncPath('files') . $filename, $this->_csvKeys);

            $nextPosition = end($parseData);
            array_pop($parseData);

            foreach ($parseData as $data) {

                $this->checkCallBackParam($data, $id);

                /* array productData */
                $productData = $this->setProduct($data);
                if ($productData) {
                    $productId = $this->_modelProduct->saveProduct($productData);
                }

                $this->setCallBackParam($data, $productId);

                /* file Options */


                /* set attribute */
            }

            $this->_modelFilesToImport->load($id)->setOptions(json_encode($this->_modelFilesToImport->getAllOptionParams()))->save();

            $position = ($nextPosition == 'end') ? $position = $totalRows : (int) $nextPosition + 1;

            $processData = array('id' => $id, 'filename' => $filename, 'position' => $position);

            try {
                /***$this->_modelFilesToImport->load($id)->addData($processData)->setId($id)->save();*/
                $this->_modelFilesToImport->setData($processData)->save();
                $result = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        return $result;
    }

    public function setCallBackParam($data, $productId) {
        if ((strlen($data['brand']) > 2)) {
            $this->_modelFilesToImport->setOptionParamsByKey('brand', $data['brand']);
        }


        if (strlen($data['sku']) > 2) {
            $childProductIds = (array)$this->_modelFilesToImport->getOptionParamsByKey('child_product_ids');
            /*** $this->_helper->saveLog('synchronization.txt', json_encode($this->_modelFilesToImport->getAllOptionParams()), true, 'All childProductIds()');
            $this->_helper->saveLog('synchronization.txt', json_encode($childProductIds), true, 'Real childProductIds()'); */
            $childProductIds[] = $productId;

            /* $this->_helper->saveLog('synchronization.txt', json_encode($childProductIds) . '---' . $productId, true, 'childProductIds()'); */

            $this->_modelFilesToImport->setOptionParamsByKey('child_product_ids', $childProductIds);
        } elseif (strlen($data['grouped']) > 2) {

            $this->_modelFilesToImport->setOptionParamsByKey('group_product_id', $productId);
            $this->_modelFilesToImport->setOptionParamsByKey('grouped', $data['grouped']);
        }
    }

    public function checkCallBackParam($data, $fileId) {
        if ($this->_modelFilesToImport->getCountOptionParams() < 1) {
            return false;
        }

        /*$this->_helper->saveLog('synchronization.txt', $this->_modelFilesToImport->getCountOptionParams(), true, 'getCountOptionParams');*/

        if(($this->_modelFilesToImport->getOptionParamsByKey('grouped') != $data['grouped']) && (strlen($data['grouped']) > 2)) {
            $this->fixedCallBackParam($fileId);
        }

         /***array(
             'barnd' => '',
             'grouped' => '',
             'grouped_id' => '',
             'child_ids' => array(),
         )   */

        return true;
    }

    public function fixedCallBackParam($fileId) {
        try {
            $this->_modelFilesToImport->load($fileId);
            $this->_modelProduct->addChildToGroupedProductById($this->_modelFilesToImport->getOptionParamsByKey('group_product_id'), $this->_modelFilesToImport->getOptionParamsByKey('child_product_ids'));
            $this->_modelFilesToImport->clearOptionParamsByKey('child_product_ids');

        } catch (Exception $ex) {

        }
    }

    public function setProduct($data) {
        /*** array('brand', 'grouped', 'sku', 'name', 'grouped_sku', 'mililitru', 'vud', 'pol', 'price') */

        $attributeSet = 4; /* Dummy */
        $categoryId = 37; /* Dummy */

        if ((strlen($data['sku']) < 2) && (strlen($data['grouped']) < 2)) {
            return false;
        }
        $productData = $this->getProductData();

        $productData['attribute_set_id'] = $attributeSet;
        $productData['category_ids'] = $categoryId;

        if (strlen($data['grouped_sku']) > 1) {
            $productData['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
            $productData['type_id'] = 'grouped';
            $productData['sku'] = $data['grouped_sku'];
            $productData['name'] = $data['grouped'];
        } else {
            $productData['name'] = $data['name']; /* TODO check if empty*/
            $productData['sku'] = $data['sku'];
            $productData['price'] = $data['price'];
        }

        return $productData;
    }

    public function setProduct1($parseData) {

        $id = $this->_modelFilesToImport->getFileToImport()->getId();

        /*** $optionsData = array(); */
        $child_product_skus = array();

        foreach ($parseData as $data) {

            $productData = $this->getProductData();

            $attributeSet = 4;
            $categoryId = 37;

            if ($data['Бренд'] != '') {
                /* $optionsData[$helper->cyr2lat('Бренд')] = $data['Бренд']; */
                $this->_modelFilesToImport->setOptionParams($helper->cyr2lat('Бренд'), $data['Бренд']);
            } else if ($data['Груп. Товар'] != '') {
                /* $optionsData[$helper->cyr2lat('Груп. Товар')] = $data['Груп. Товар'];
                $optionsData['group_product_sku'] = $data['']; */

                $this->_modelFilesToImport->setOptionParams($helper->cyr2lat('Груп. Товар'), $data['Груп. Товар']);
                $this->_modelFilesToImport->setOptionParams('group_product_sku', $data['']);


                $productData['attribute_set_id'] = $attributeSet;
                $productData['category_ids'] = $categoryId;
                $productData['name'] = $data['Груп. Товар'];
                $productData['sku'] = $data[''];
                $productData['type_id'] = 'grouped';
                $productData['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;

                /* $optionsData['group_product_id'] = $this->_modelProduct->saveProduct($productData); */

                $this->_modelFilesToImport->setOptionParams('group_product_id', $this->_modelProduct->saveProduct($productData));

                $child_product_skus = '';

            } else if ($attributeSet && isset($categoryId) && $data['Артикул'] != '') {

                /* Product Part */
                $productData['attribute_set_id'] = $attributeSet;
                $productData['category_ids'] = $categoryId;
                $productData['name'] = $data['Наименование'];
                $productData['sku'] = $data['Артикул'];
                $productData['price'] = $data['Цена'];
                /* $this->productData['ml'] = $data['Мл'];
                  $this->productData['vyd'] = $data['Вид'];
                  $this->productData['pol'] = $data['Пол'];
                 */

                $this->_modelProduct->saveProduct($productData);
                $child_product_skus[] = $data['Артикул'];
                /* $optionsData['child_product_skus'] = $child_product_skus; */
                $this->_modelFilesToImport->setOptionParams('child_product_skus', $child_product_skus);
            }
        }

        $this->_modelFilesToImport->load($id)->setOptions(json_encode($this->_modelFilesToImport->getAllOptionParams()))->save();

        $fileOptions = json_decode($this->_modelFilesToImport->load($id)->getOptions(), true);

        $this->_modelProduct->addChildToGroupedProduct($fileOptions['group_product_id'], $fileOptions['child_product_skus']);
    }


}
