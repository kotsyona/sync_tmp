<?php

class Wplg_Synchronization_Model_Export extends Mage_Core_Model_Abstract {

    protected $_helper;
    protected $_subPathForProductExportFiles;
    protected $_subPathForCategoriesExportFiles;
    protected $_subPathForProductTypesExportFiles;
    protected $_attributes;

    /**
     * @return mixed
     */
    public function getAttributes() {
        return $this->_attributes;
    }

    /**
     * @return mixed
     */
    public function getSubPathForProductTypesExportFiles() {
        return $this->_subPathForProductTypesExportFiles;
    }

    /**
     * @return mixed
     */
    public function getSubPathForCategoriesExportFiles() {
        return $this->_subPathForCategoriesExportFiles;
    }

    /**
     * @return mixed
     */
    public function getSubPathForProductExportFiles() {
        return $this->_subPathForProductExportFiles;
    }

    protected function _construct() {
        $this->_helper = Mage::helper('synchronization');
        $this->_subPathForProductExportFiles = 'export' . DS . 'products';
        $this->_subPathForCategoriesExportFiles = 'export' . DS . 'categories';
        $this->_subPathForProductTypesExportFiles = 'export' . DS . 'product_types';
        $this->_attributes = array('KodKategorii' => 'category_ids', 'ProductType' => 'attribute_set', 'Artikul' => 'sku', 'Naimenovanie' => 'name', 'Price' => 'price', 'Kolichestvo' => 'quantity', 'ImageFolder' => 'image_folder', 'Image' => 'image');
    }

    protected function _getAllAttributesForExport() {
        $attributes = $this->getAttributes();

        $attributesFromConfig = Mage::getStoreConfig('wplgsynchronization/exportwplgsynchronization/attribute_codes_for_export', Mage::app()->getStore());
        if (!empty($attributesFromConfig)) {
            $attributesFromConfigCodes = explode(',', $attributesFromConfig);
            foreach ($attributesFromConfigCodes as $attributeCode) {
                if(in_array($attributeCode, $attributes)) {
                    continue;
                }
                $attributeData = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', $attributeCode);
                if ($attributeData->getId() !== null) {
                    $frontendInput = Mage::getModel('synchronization/attribute')->getFrontendInputTypeForExport($attributeData->getFrontendInput());
                    $frontendInput = (strlen($frontendInput) > 0) ? $frontendInput : $attributeData->getFrontendInput();
                    $fontendLabel = (strlen($attributeData->getFrontendLabel()) > 0 ? $attributeData->getFrontendLabel() : $attributeCode);
                    $attributes[$fontendLabel . '|' . $frontendInput] = $attributeCode;
                }
            }
        }
        return $attributes;
    }

    public function exportProductsToFile($categoriesIds) {
        $result = false;

        if (is_array($categoriesIds)) {
            try {
                $messages = array();
                $path = $this->_helper->getSyncPath($this->getSubPathForProductExportFiles());

                /* Delete all product export files from directory */
                $this->_helper->deleteAllFilesFromPath($path);

                foreach ($categoriesIds as $categoriesId) {
                    $categoriesId = preg_replace('/\D/', '', $categoriesId);
                    $category = Mage::getModel('catalog/category')->load($categoriesId);

                    if ($category->getId() === null) {
                        continue;
                    }

                    $productModel = Mage::getModel('catalog/product');
                    Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
                    $fileName = date('Y-m-d_H-i-s', Mage::getModel('core/date')->timestamp(time())) . '_' . $this->_helper->cyr2lat($category->getName()) . '.csv';
                    $delimiter = ',';

                    $attributes = $this->_getAllAttributesForExport();

                    $header = array_keys($attributes);

                    /* load products */
                    $products = Mage::getModel('catalog/product')
                        ->getCollection()
                        ->addAttributeToSelect('*')
                        ->addCategoryFilter($category)
                        ->setPageSize(50);

                    if ($products->getSize() == 0) {
                        $messages[] = $this->_helper->__("Synchronization, In category '%s' no records found for export.", $category->getName());
                        continue;
                    }

                    $pages = $products->getLastPageNumber();

                    /* Create dir if not exist */
                    if (!is_dir($path)) {
                        mkdir($path, 0755, true);
                    }
                    /* open the csv file and write the header in the first line */
                    $fp = fopen($path . $fileName, 'w');

                    fputcsv($fp, $header, $delimiter);

                    /* iterate through all the products */
                    $currentPage = 1;

                    do {
                        $products->setCurPage($currentPage);
                        $products->load();

                        foreach ($products as $_product) {
                            $product_row = array();

                            foreach ($attributes as $attribute) {
                                $values = array();

                                switch ($attribute) {
                                    case "image":
                                        $items = $mediaApi->items($_product->getId());
                                        foreach ($items as $item) {
                                            $values[] = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'catalog' . DS . 'product' . $item['file'];
                                        }
                                        break;
                                    case "category_ids":
                                        $values[] = implode(',', $_product->getCategoryIds());
                                        break;
                                    case "quantity":
                                        $values[] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product)->getQty() * 1;
                                        break;
                                    case "attribute_set":
                                        $attributeSetModel = Mage::getModel("eav/entity_attribute_set");
                                        $attributeSetModel->load($_product->getAttributeSetId());
                                        $values[] = $attributeSetModel->getAttributeSetID();
                                        break;
                                    default:
                                        $attr = $productModel->getResource()->getAttribute($attribute);
                                        if (Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', $attribute)->getId() !== null) {
                                            if ($attr->usesSource() && $attr['frontend_input'] = "multiselect") {
                                                foreach (explode(',', $_product->getData($attribute)) as $optionId) {
                                                    $values[] = $attr->getSource()->getOptionText($optionId);
                                                }
                                            } else {
                                                $values[] = $_product->getData($attribute);
                                            }
                                        } else {
                                            $values[] = '';
                                        }
                                        break;
                                }

                                $product_row[] = implode(',', $values);
                            }

                            fputcsv($fp, $product_row, $delimiter);
                        }

                        $currentPage++;
                        /* clear collection and free memory */
                        $products->clear();
                    } while ($currentPage <= $pages);

                    fclose($fp);

                    if (file_exists($path . $fileName)) {
                        $messages[] = $this->_helper->__("Synchronization, The file export for category '%s' has been created.", $category->getName());
                    }
                }

                $result = true;
                Mage::getSingleton('core/session')->setMessageExportProducts(serialize($messages));
            } catch (Exception $ex) {
                Mage::getSingleton('adminhtml/session')->addError($this->_helper->__('An error occurred while creating the product export file.'));
                echo $ex;
            }
        }

        return $result;
    }

    public function exportCategoriesToFile() {
        $result = false;
        try {
            $messages = array();
            $path = $this->_helper->getSyncPath($this->getSubPathForCategoriesExportFiles());

            /* Delete all category export files from directory */
            $this->_helper->deleteAllFilesFromPath($path);

            $fileName = date('Y-m-d_H-i-s', Mage::getModel('core/date')->timestamp(time())) . '_categories.csv';
            $delimiter = ',';

            $categoriesDataForExport = Mage::getModel('synchronization/categorysynchronization')->prepareCategoriesDataForExport();

            $header = array('NameLocalCategory', 'IdLocalCategory', 'MagentoId');

            if (count($categoriesDataForExport) == 0) {
                $messages[] = $this->_helper->__("Synchronization, No records found for export.");
                return $result;
            }

            /* Create dir if not exist */
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            /* open the csv file and write the header in the first line */
            $fp = fopen($path . $fileName, 'w');

            fputcsv($fp, $header, $delimiter);

            foreach ($categoriesDataForExport as $categoryRow) {
                fputcsv($fp, $categoryRow, $delimiter);
            }

            fclose($fp);

            if (file_exists($path . $fileName)) {
                $messages[] = $this->_helper->__("Synchronization, The file export for categories has been created.");
            }

            $result = true;
            Mage::getSingleton('core/session')->setMessageExportCategories(serialize($messages));
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError($this->_helper->__('An error occurred while creating the categories export file.'));
            echo $ex;
        }

        return $result;
    }

    public function exportProductTypesToFile() {
        $result = false;
        try {
            $messages = array();
            $path = $this->_helper->getSyncPath($this->getSubPathForProductTypesExportFiles());

            /* Delete all category export files from directory */
            $this->_helper->deleteAllFilesFromPath($path);

            $fileName = date('Y-m-d_H-i-s', Mage::getModel('core/date')->timestamp(time())) . '_product_types.csv';
            $delimiter = ',';

            $productTypesDataForExport = Mage::getModel('synchronization/producttypesynchronization')->prepareProductTypesDataForExport();

            $header = array('NameLocalProductType', 'IdLocalProductType', 'MagentoId');

            if (count($productTypesDataForExport) == 0) {
                $messages[] = $this->_helper->__("Synchronization, No records found for export.");
                return $result;
            }

            /* Create dir if not exist */
            if (!is_dir($path)) {
                mkdir($path, 0755, true);
            }
            /* open the csv file and write the header in the first line */
            $fp = fopen($path . $fileName, 'w');

            fputcsv($fp, $header, $delimiter);

            foreach ($productTypesDataForExport as $productTypeRow) {
                fputcsv($fp, $productTypeRow, $delimiter);
            }

            fclose($fp);

            if (file_exists($path . $fileName)) {
                $messages[] = $this->_helper->__("Synchronization, The file export for product types has been created.");
            }

            $result = true;
            Mage::getSingleton('core/session')->setMessageExportProductTypes(serialize($messages));
        } catch (Exception $ex) {
            Mage::getSingleton('adminhtml/session')->addError($this->_helper->__('An error occurred while creating the product types export file.'));
            echo $ex;
        }

        return $result;
    }

}
