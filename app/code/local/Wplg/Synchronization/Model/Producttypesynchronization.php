<?php

class Wplg_Synchronization_Model_Producttypesynchronization extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("synchronization/producttypesynchronization");
    }

    /*
     * Get magento id product type by local id product type
     *
     * @param int $localIdProductType
     */
    public function getMagentoIdProductTypeByLocalIdProductType($localIdProductType) {
        $collection = $this->getCollection()
                ->addFieldToSelect('id_site_product_type')
                ->addFieldToFilter('id_local_product_type', $localIdProductType);


        $attribute_set = Mage::getModel("eav/entity_attribute_set")->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter("attribute_set_id", $collection->getData('id_site_product_type'));
        
        if ($attribute_set->getData()) {
            return $collection->getData();
        } else {
            return NULL;
        }
    }

    /**
     * Prepare product types data for export
     */
    public function prepareProductTypesDataForExport() {
        $productTypesDataForExport = false;
        try {
            $productTypesSynchronization = $this->getCollection()
                ->addFieldToSelect('name_local_product_type')
                ->addFieldToSelect('id_local_product_type')
                ->addFieldToSelect('id_site_product_type');

            $siteProductTypesIds = array();

            $attributeSets = Mage::getModel('catalog/product_attribute_set_api')->items();

            if (count($attributeSets) > 0) {
                foreach ($attributeSets as $attributeSet) {
                    $siteProductTypesIds[] = $attributeSet['set_id'];
                }
            }

            foreach ($productTypesSynchronization as $productType) {
                $productTypesDataForExport[] = array($productType->getNameLocalProductType(), $productType->getIdLocalProductType(), $productType->getIdSiteProductType());
                $key = array_search($productType->getIdSiteProductType(), $siteProductTypesIds);
                if ($key !== false) {
                    unset($siteProductTypesIds[$key]);
                }

            }

            foreach ($siteProductTypesIds as $siteProductTypeId) {
                $productTypesDataForExport[] = array('', '', $siteProductTypeId);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
        return $productTypesDataForExport;
    }

}
