<?php

class Wplg_Synchronization_Model_Adapters1kotsyonaitwebodua extends Mage_Core_Model_Abstract {

    protected $_productData = array();
    /*Бренд	Груп. Товар	Артикул	Наименование		Мл	Вид	Пол	Цена    Папка с изображениями   Изображения*/
    protected $_csvKeys = array('brand', 'grouped', 'sku', 'name', 'grouped_sku', 'mililitru', 'vud', 'pol', 'price', 'folder_name', 'image_names');
    protected $_modelFilesToImport;
    protected $_modelProduct;
    protected $_helper;
    protected $attribute_set_ids = array(4);

    protected function _construct() {
        $this->_setProductData();
        $this->_modelFilesToImport = Mage::getModel("synchronization/filestoimport");
        $this->_modelProduct = Mage::getModel("synchronization/product");
        $this->_helper = Mage::helper('synchronization');
    }

    protected function _setProductData() {
        $this->_productData = array(
            'attribute_set_id' => 4,
            'weight' => 1,
            'price' => 0,
            'status' => 1,
            'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
            'is_active' => 1,
            'type_id' => 'simple',
            'tax_class_id' => '0',
            'category_ids' => 2, /* ToDo*/
            'website_ids' => 1,
            /*'store_id' => 1,
            'is_in_stock' => 1,*/
            /* 'qty' => 1000, */ /* ToDo*/
        );
    }

    public function getProductData() {
        return $this->_productData;
    }

    public function parseFile() {
        $result = false;
        $file = $this->_modelFilesToImport->getFileToImport();

        $fileId = $file->getId();
        $position = $file->getPosition();
        $filename = $file->getFilename();
        $totalRows = $file->getTotalRows();

        /* initial*/
        $this->_modelFilesToImport->setOptionParams(json_decode($this->_modelFilesToImport->load($fileId)->getOptions(), true));

        $parseAtOnceFromConfig = Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/parseatonce', Mage::app()->getStore());
        $parseAtOnce = empty($parseAtOnceFromConfig) ? 1 : $parseAtOnceFromConfig;

        $endPosition = $position + $parseAtOnce;

        while ($position < $endPosition && $position != $totalRows && !empty($filename)) {

            $parseResult = $this->_helper->some_csv_lines_to_array($position, $parseAtOnce, $this->_helper->getSyncPath('files') . $filename, $this->_csvKeys);

            $parseData = $parseResult[0];
            $nextPosition = $parseResult[1];

            foreach ($parseData as $data) {
                $this->checkCallBackParam($data, $fileId);
                $this->setAttributesToGroupProduct($data);
                $this->setAttributesToSimpleProduct($data);

                /* array productData */
                $productData = $this->setProduct($data);
                if ($productData) {
                    $productId = $this->_modelProduct->saveProduct($productData);
                    /* save images to product */
                    /* if (strlen(trim($data['folder_name'])) > 0) {
                        if (strlen(trim($data['image_names'])) > 0) {
                            $up_img = $this->_helper->getFilesImgFromCsv($this->_helper->getUrlPathForImage(trim($data['folder_name'])), $data['image_names']);
                        } else {
                            $up_img = $this->_helper->getFilesImgBySku($this->_helper->getUrlPathForImage(trim($data['folder_name'])), $productData['sku']);
                        }
                        $this->_modelProduct->saveProductImages($up_img, $productId, $productData['sku']);
                    } */
                }

                $this->setCallBackParam($data, $productId);

                $this->_modelFilesToImport->load($fileId)->setOptions(json_encode($this->_modelFilesToImport->getAllOptionParams()))->save();
            }

            $position = ($nextPosition == 'end') ? $position = $totalRows : (int) $nextPosition + 1;
            $processData = array('id' => $fileId, 'filename' => $filename, 'position' => $position);

            try {
                $this->_modelFilesToImport->setData($processData)->save();
                $result = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        return $result;
    }

    public function setCallBackParam($data, $productId) {
        if ((strlen($data['brand']) > 2)) {
            $this->_modelFilesToImport->setOptionParamsByKey('brand', trim($data['brand']));
        }

        if (strlen($data['sku']) > 2 && $productId > 0) {
            $childProductIds = (array)$this->_modelFilesToImport->getOptionParamsByKey('child_product_ids');
            $childProductIds[] = $productId;
            $this->_modelFilesToImport->setOptionParamsByKey('child_product_ids', $childProductIds);
            $this->_modelFilesToImport->setOptionParamsByKey('simple_product_id', $productId);
        } elseif (strlen($data['grouped']) > 2 && $productId > 0) {
            $this->_modelFilesToImport->setOptionParamsByKey('group_product_id', $productId);
            $this->_modelFilesToImport->setOptionParamsByKey('grouped', trim($data['grouped']));
        }
    }

    public function checkCallBackParam($data, $fileId) {
        if ($this->_modelFilesToImport->getCountOptionParams() < 1) {
            return false;
        }

        if(($this->_modelFilesToImport->getOptionParamsByKey('grouped') != trim($data['grouped'])) && (strlen($data['grouped']) > 2)) {
            $this->fixedCallBackGroupProductParam($fileId);
        } elseif (trim($data['brand']) != $this->_modelFilesToImport->getOptionParamsByKey('brand') && (strlen($data['brand']) > 2)){
            $this->fixedCallBackGroupProductParam($fileId);
        } else if (strlen($this->_modelFilesToImport->getOptionParamsByKey('simple_product_id')) > 0) {
            $this->fixedCallBackSimpleProductParam();
        }

        return true;
    }

    public function fixedCallBackGroupProductParam($fileId) {
        try {
            $this->_modelFilesToImport->load($fileId);
            $this->_modelFilesToImport->setOptionParams(json_decode($this->_modelFilesToImport->getOptions(), true));
            $groupedId = $this->_modelFilesToImport->getOptionParamsByKey('group_product_id');
            if(strlen($groupedId) > 0 && count($this->_modelFilesToImport->getOptionParamsByKey('attributes')) == 2) {
                $this->_modelProduct->addChildToGroupedProductById($groupedId, $this->_modelFilesToImport->getOptionParamsByKey('child_product_ids'));
                $this->_modelFilesToImport->clearOptionParamsByKey('child_product_ids');
                Mage::getModel("synchronization/attribute")->addAttributesToMage($this->_modelFilesToImport->getOptionParamsByKey('attributes'), $this->_attribute_set_ids);
                Mage::getModel("synchronization/attroptions")->addattroptionsToMage($this->_modelFilesToImport->getOptionParamsByKey('attributes'));
                $this->_modelProduct->saveProductAttributesOptions($groupedId, $this->_modelFilesToImport->getOptionParamsByKey('attributes'));
                /*** $this->_helper->saveLog('synchronization.txt', json_encode($this->_modelFilesToImport->getOptions()), true, ' - '); */
                $this->_modelFilesToImport->clearOptionParamsByKey('attributes');
            }
        } catch (Exception $ex) {
            $this->_helper->saveLog('synchronization.txt', $ex, true, 'function fixedCallBackGroupProductParam ex : ');
        }
    }

    public function fixedCallBackSimpleProductParam() {
        try {
            $simpleId = $this->_modelFilesToImport->getOptionParamsByKey('simple_product_id');
            if (strlen($simpleId) > 0) {
                Mage::getModel("synchronization/attribute")->addAttributesToMage($this->_modelFilesToImport->getOptionParamsByKey('simple_attributes'));
                Mage::getModel("synchronization/attroptions")->addattroptionsToMage($this->_modelFilesToImport->getOptionParamsByKey('simple_attributes'));
                $this->_modelProduct->saveProductAttributesOptions($simpleId, $this->_modelFilesToImport->getOptionParamsByKey('simple_attributes'));
            }
        } catch (Exception $ex) {
            $this->_helper->saveLog('synchronization.txt', $ex, true, 'function fixedCallBackSimpleProductParam ex : ');
        }
    }

    public function setProduct($data) {
        /*** array('brand', 'grouped', 'sku', 'name', 'grouped_sku', 'mililitru', 'vud', 'pol', 'price') */

        $attributeSet = 4; /* Dummy *//*4*/
        $categoryId = 8; /* Dummy *//*8*/

        if ((strlen($data['sku']) < 2) && (strlen($data['grouped']) < 2)) {
            return false;
        }
        $productData = $this->getProductData();

        $productData['attribute_set_id'] = $attributeSet;
        $productData['category_ids'] = $categoryId;

        if (strlen($data['grouped_sku']) > 1) {
            $productData['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
            $productData['type_id'] = 'grouped';
            $productData['sku'] = $data['grouped_sku'];
            $productData['name'] =  trim($data['grouped']);
        } else {
            $productData['name'] = trim($data['name']); /* TODO check if empty*/
            $productData['sku'] = $data['sku'];
            $productData['price'] = $data['price'];
        }

        return $productData;
    }

    public function setAttributesToGroupProduct($data) {
        $attributes = (array) $this->_modelFilesToImport->getOptionParamsByKey('attributes');

        if (strlen($data['brand']) > 0) {
            $attributes['manufacturer_o'] = trim($data['brand']);
            $this->_modelFilesToImport->setOptionParamsByKey('brand', trim($data['brand']));
        } elseif ((strlen($this->_modelFilesToImport->getOptionParamsByKey('brand')) > 2)) {
            $attributes['manufacturer_o'] = $this->_modelFilesToImport->getOptionParamsByKey('brand');
        }
        if (strlen($data['pol']) > 0) {
            if($data['pol'] == "Ж") {
                $attributes['sex_pol'] = "Jenskaja";
            } elseif($data['pol'] == "М" || $data['pol'] == "M") {
                $attributes['sex_pol'] = "Mujskaja";
            } else {
                $attributes['sex_pol'] = array('Jenskaja', 'Mujskaja');
            }
        }

        /*** array( 'manufacturer_o' => 'ARMAND BASI', 'sex_pol' => 'Женская'; */
        $this->_modelFilesToImport->setOptionParamsByKey('attributes', $attributes);
    }

    public function setAttributesToSimpleProduct($data) {
        $simpleAttributes = (array) $this->_modelFilesToImport->getOptionParamsByKey('simple_attributes');

        if (strlen($data['mililitru']) > 0 && strlen($data['vud']) > 0) {
            $simpleAttributes['milliliters'] = trim($data['vud']) . ' ' . trim($data['mililitru']);
        }

        /*** array( 'milliliters' => 'EDT 50'; */
        $this->_modelFilesToImport->setOptionParamsByKey('simple_attributes', $simpleAttributes);
    }

}
