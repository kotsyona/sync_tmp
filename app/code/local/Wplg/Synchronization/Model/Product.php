<?php

class Wplg_Synchronization_Model_Product extends Mage_Core_Model_Abstract {

    /**
     * Save product
     *
     * @param array $productData
     * @return boolen
     */
    public function saveProduct($productData, $qty = 1000) {
        try {
            $product = new Mage_Catalog_Model_Product();
            $product->setData($productData);
            $product->setWebsiteIds(array(1));
            $product->save();
            $product = $product->load($product->getId());
            $product->setStockData(array(
                              'manage_stock' => 1,
                              'is_in_stock' => 1,
                              'qty' => $qty,
                              'use_config_manage_stock' => 1,
                              'use_config_enable_qty_increments' => 1
            ));

            $product->save();
            return $product->getId();
        } catch (Exception $ex) {
            Mage::helper("synchronization")->saveLog('synchronization.txt', $ex, true, 'function saveProduct ex : ');
            return false;
        }
    }

    public function saveProductImages($imagesPath, $imagesNames, $productId, $sku) {
        $imagesPath = DS . $imagesPath . DS;

        $imageFileNames = array();
        foreach ($imagesNames as $key => $name) {
            $imageName = trim($name);

            if (file_exists($imagesPath . $imageName)) {
                if($key === 0) {
                    $imageFileNames["main"] = $imageName;
                } else {
                    $arrayKey = "z". $key;
                    $imageFileNames[$arrayKey] = $imageName;
                }
            }
            ksort($imageFileNames);
        }

        if (count($imageFileNames) > 0) {
            $product = Mage::getModel('catalog/product')->load($productId);

            /* Delete existing images */
            Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            $items = $mediaApi->items($productId);
            $attributes = $product->getTypeInstance()->getSetAttributes();
            $gallery = $attributes['media_gallery'];
            foreach ($items as $item) {

                if (in_array(substr($item['file'], 5), $imageFileNames)) {
                    $gallery->getBackend()->removeImage($product, $item['file']);
                    $fileName = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'product' . $item['file'];
                    if (file_exists($fileName)) {
                        unlink($fileName);
                    }

                }

            }
            $product->save();

            $product = Mage::getModel('catalog/product')->load($productId);
            $product->setMediaGallery(array('images' => array(), 'values' => array()));

            foreach ($imageFileNames as $key => $fileName) {
                if ((string)$key === "main") {
                    $status = array('image', 'thumbnail', 'small_image');
                } else {
                    $status = array('');
                }
                if (file_exists($imagesPath . $fileName)){

                    /* TODO if config to rename,  SKU to file name */
                    /* $fileName = (rename($fileName, dirname($fileName) . DS . $sku . '_' . basename($fileName))) ? dirname($fileName) . DS . $sku . '_' . basename($fileName) : $fileName; */
                    $product->addImageToMediaGallery($imagesPath . $fileName, $status, true, false); /* The third parameter must be TRUE for remove the imported images */
                }
            }

            $product->save();
        }
    }


    /* TODO Check if exist new  foto, afte delete */
    public function saveProductImages1($up_img, $productId, $sku) {
        $product = Mage::getModel('catalog/product')->load($productId);

        /* Delete existing images */
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
        $items = $mediaApi->items($productId);
        $attributes = $product->getTypeInstance()->getSetAttributes();
        $gallery = $attributes['media_gallery'];
        foreach ($items as $item) {

            /* CHECK */

            /* TODO if (in_array($up_img, $item['file'])) */

            /**** Mage::helper("synchronization")->saveLog('synchronization.txt', serialize($up_img), true, 'function saveProduct ex : '); */

            /* TODO Check if exist new  foto, afte delete */
            /*$gallery->getBackend()->removeImage($product, $item['file']);
            $fileName = Mage::getBaseDir(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . 'catalog' . DS . 'product' . $item['file'];
            if (file_exists($fileName)) {
                unlink($fileName);
            }*/
        }
        $product->save();

        /* Add an image from the list */

        $product = Mage::getModel('catalog/product')->load($productId);
        $product->setMediaGallery(array('images' => array(), 'values' => array()));

        foreach ($up_img as $key => $fileName) {
            if ((string)$key === "main") {
                $status = array('image', 'thumbnail', 'small_image');
            } else {
                $status = array('');
            }
            if (file_exists($fileName)){

                /* TODO if config to rename */
                /* $fileName = (rename($fileName, dirname($fileName) . DS . $sku . '_' . basename($fileName))) ? dirname($fileName) . DS . $sku . '_' . basename($fileName) : $fileName; */
                $product->addImageToMediaGallery($fileName, $status, true, false); /* The third parameter must be TRUE for remove the imported images */
            }
        }

        $product->save();
    }

    /**
     * Save product attributes options
     *
     * @param int $productId
     * @param array $attributes
     * @return bool
     *
     */
    public function saveProductAttributesOptions($productId, $attributes) {
        if (strlen($productId) > 0) {
            try {
                $product = Mage::getModel("catalog/product")->load($productId);
                foreach ($attributes as $attributeKey => $attributeValue) {
                    $sourceModel = $product->getResource()->getAttribute($attributeKey)->getSource();
                    if (is_array($attributeValue)) {
                        $optionIds = array_map(array($sourceModel, 'getOptionId'), $attributeValue);
                        $product->setData($attributeKey, implode(",", $optionIds));
                    } else {
                        $optionId = $sourceModel->getOptionId($attributeValue);
                        $product->setData($attributeKey, $optionId);
                    }
                    $product->save();
                }
                return $product->getEntityId();
            } catch (Exception $ex) {
                return false;
            }
        }

        return true;
    }

    /**
     * Save product attributes options by option ids
     *
     * @param int $productId
     * @param array $attributes  array([cvet] => 87,13,5,83)
     * @return bool
     *
     */

    public function saveProductAttributesOptionsByOptionIds($productId, $attributes) {

        Mage::helper("synchronization")->saveLog('synchronization.txt', serialize($attributes), true, '');

        if (strlen($productId) > 0) {
            try {
                $product = Mage::getModel("catalog/product")->load($productId);
                foreach ($attributes as $attributeKey => $attributeIds) {
                    /* TODO check type attribute */

                    $modelAttribute = Mage::getModel("synchronization/attribute");

                    switch($modelAttribute->getTypeAttributeByCode($attributeKey)){
                        case 'select':
                            $product->setData($attributeKey, $attributeIds[0]);
                            break;
                        case 'multi':
                            $product->setData($attributeKey, $attributeIds);
                            break;
                        case 'string':
                            /* $product->setData($attributeKey, $attributeIds[0]); */
                            break;
                    }


                    $product->save();
                }
                return $product->getEntityId();
            } catch (Exception $ex) {
                return false;
            }
        }
    }

    /*
     * Add simple products to grouped product
     *
     * @param int $groupedProductId
     * @param array $listChildProductIds
     */
    public function addChildToGroupedProductById($groupedProductId, $listChildProductIds) {
        foreach ($listChildProductIds as $childProductId) {
            Mage::getModel('catalog/product_link_api')->assign("grouped", $groupedProductId, $childProductId);
        }
    }

}
