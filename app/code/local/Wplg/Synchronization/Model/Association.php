<?php

class Wplg_Synchronization_Model_Association extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("synchronization/association");
    }

    public function checkIfExistInAssociationTable($type, $key) {
        $result = false;
        $collection = $this->getCollection()
                ->addFieldToSelect('*')
                ->addFieldToFilter('a_type', $type)
                ->addFieldToFilter('a_key', $key);

        if ($collection->getData('id')) {
            $result = true;
        }

        return $result;
    }

    public function addToAssociationTable($type, $key, $value) {
        try {
            $this->setData(
                    array(
                        'a_type' => $type,
                        'a_key' => $key,
                        'a_value' => $value
                    )
            )->save();
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    public function getValueByTypeAndKeyFromAssociation($type, $key) {
        $result = false;
        $collection = $this->getCollection()
                ->addFieldToSelect('a_value')
                ->addFieldToFilter('a_type', $type)
                ->addFieldToFilter('a_key', $key);

        if ($collection->getFirstItem()->getData('a_value')) {
            $result = $collection->getFirstItem()->getData('a_value');
        }

        return $result;
    }

    public function getKeysByTypeAndValue($type, $value) {
        $result = false;
        $collection = $this->getCollection()
            ->addFieldToSelect('a_key')
            ->addFieldToFilter('a_type', $type)
            ->addFieldToFilter('a_value', $value);

        if ($collection->getSize() > 0) {
            $result = $collection;
        }

        return $result;
    }

    public function cleanGarbage($type, $key = '', $value = '') {
        $collection = $this->getCollection()
            ->addFieldToSelect('*');

        if (strlen($type) > 0) {
            $collection->addFieldToFilter('a_type', $type);
        }

        if (strlen($key) > 0) {
            $collection->addFieldToFilter('a_key', $key);
        }

        if (strlen($value) > 0) {
            $collection->addFieldToFilter('a_value', $value);
        }

        $rows = $collection->getData();

        try {
            foreach ($rows as $row) {
                $this->load($row['id'])->delete();
            }
            return TRUE;
        } catch (Exception $exc) {
            echo $exc;
            return FALSE;
        }


    }


}
