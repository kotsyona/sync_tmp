<?php

class Wplg_Synchronization_Model_Adapter extends Mage_Core_Model_Abstract {

    protected $_productData = array();
    /*Код категории	Тип продукта    Артикул    Имя       Цена   Количество    Папка с изображениями   Изображения   Цвет   Тип столешницы*/
    /*protected $_csvKeys = array('kod_kategorii', 'product_type', 'sku', 'name', 'price', 'qty', 'folder_name', 'image_names', 'cvet', 'tip_stoleshnicy');*/
    protected $_csvKeys;
    protected $_modelFilesToImport;
    protected $_modelProduct;
    protected $_modelAttributeOptions;
    protected $_helper;
    protected $attribute_set_ids = array(4);
    protected $_modelAssociation;

    protected function _construct() {
        $this->_setProductData();
        $this->_modelFilesToImport = Mage::getModel("synchronization/filestoimport");
        $this->_modelProduct = Mage::getModel("synchronization/product");
        $this->_modelAttributeOptions = Mage::getModel("synchronization/attroptions");
        $this->_helper = Mage::helper('synchronization');
        $this->_csvKeys = $this->_helper->getNamesOfColumnsInFileFromConfig();
        $this->_modelAssociation = Mage::getModel("synchronization/association");
    }

    protected function _setProductData() {
        $this->_productData = array(
          'attribute_set_id' => 4,
          'weight' => 1,
          'price' => 0,
          'status' => 1,
          'visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE,
          'is_active' => 1,
          'type_id' => 'simple',
          'tax_class_id' => '0',
          'category_ids' => 2, /* ToDo*/
          'website_ids' => 1,
        );
    }

    public function getProductData() {
        return $this->_productData;
    }

    public function parseFile() {
        $result = false;
        $file = $this->_modelFilesToImport->getFileToImport();

        $fileId = $file->getId();
        $position = $file->getPosition();
        $filename = $file->getFilename();
        $totalRows = $file->getTotalRows();
        $attributeCodes = (array)json_decode($file->getAttributeCodes());

        $parseAtOnceFromConfig = Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/parseatonce', Mage::app()->getStore());
        $parseAtOnce = empty($parseAtOnceFromConfig) ? 1 : $parseAtOnceFromConfig;

        $endPosition = $position + $parseAtOnce;

        while ($position < $endPosition && $position != $totalRows && !empty($filename)) {

            $parseResult = $this->_helper->some_csv_lines_to_array($position, $parseAtOnce, $this->_helper->getSyncPath('files') . $filename);

            $parseData = $parseResult[0];
            $nextPosition = $parseResult[1];

            foreach ($parseData as $data) {
                /* get magento category id */
                $categoryId = Mage::getModel("synchronization/categorysynchronization")->getMagentoIdCategoryByLocalIdCategory(trim($data[0]));
                if(!empty($categoryId)) {
                    $data[0] = $categoryId[0]['id_site_category'];
                } else {
                    break;
                }

                /* get magento product type id */
                $productTypeId = Mage::getModel("synchronization/producttypesynchronization")->getMagentoIdProductTypeByLocalIdProductType(trim($data[1]));
                if(!empty($productTypeId)) {
                    $data[1] = $productTypeId[0]['id_site_product_type'];
                } else {
                    break;
                }

                /* product attribute options */
                $attributeValuesArray = array();
                foreach ($attributeCodes as $attributePosition => $attributeKeyInAssociation) {
                    $attributeValues = trim($data[$attributePosition]);
                    if (strlen($attributeValues) > 0) {
                        $attributeCode = $this->_modelAttributeOptions->getAttributeCodeByKeyInAssociation($attributeKeyInAssociation);
                        $attributeValuesArray[$attributeCode] = $attributeValues;
                    }
                }

                /* array productData */
                $productData = $this->setProduct($data);
                if ($productData) {
                    $productId = $this->_modelProduct->saveProduct($productData, trim($data[5]));
                    /* $this->_modelProduct->saveProductAttributesOptionsByOptionIds($productId, $attributesToSave); */
                    $this->_modelAttributeOptions->saveProductAttributeOptions($productId, $attributeCodes, $attributeValuesArray);

                    $imageFolder = trim($data[6]);
                    $imagesNames = trim($data[7]);

                    if ( (strlen($imageFolder) > 0) && (strlen($imagesNames) > 0)) {

                        /* TODO get path to images from config */
                        $this->_modelProduct->saveProductImages(Mage::helper('synchronization')->getUrlPathForImage($imageFolder), explode(",", $imagesNames), $productId, $productData['sku']);

                    }

                    $groupedproductSku = trim($data[8]);
                    if (strlen($groupedproductSku) > 0 && strlen($productId) > 0) {
                        if (!$this->_modelAssociation->checkIfExistInAssociationTable('configurable_simple', $productId)) {
                            $this->_modelAssociation->addToAssociationTable('configurable_simple', $productId, $groupedproductSku);
                        }
                    }

                    $productTypeId = trim($data[9]);
                    if (strlen($productTypeId) > 0 && $productTypeId == 'configurable' && strlen($productData['sku']) > 0) {
                        $associationCollection = $this->_modelAssociation->getKeysByTypeAndValue('configurable_simple', $productData['sku']);

                        $simpleIdsForConfigurable = array();
                        foreach($associationCollection->getData() as $associationData) {
                            $simpleIdsForConfigurable[] = $associationData['a_key'];
                        }

                        if($simpleIdsForConfigurable) {
                            $this->_assignSimpleToConfigurable($productId, $simpleIdsForConfigurable, $productData['sku']);
                        }
                    }
                }
            }

            $position = ($nextPosition == 'end') ? $position = $totalRows : (int) $nextPosition + 1;
            $processData = array('id' => $fileId, 'filename' => $filename, 'position' => $position);

            try {
                $this->_modelFilesToImport->setData($processData)->save();
                $result = true;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        return $result;
    }

    public function fixedCallBackGroupProductParam($fileId) {
    }

    public function fixedCallBackSimpleProductParam() {
    }

    public function setProduct($data) {
        /*** array('kod_kategorii', 'product_type', 'sku', 'name', 'price', 'qty', 'folder_name', 'image_names', 'cvet', 'tip_stoleshnicy') */

        $productTypes = array('simple', 'grouped', 'configurable');

        if ((strlen(trim($data[2])) < 2)) {
            return false;
        }
        $productData = $this->getProductData();

        $productData['attribute_set_id'] = $data[1];
        $productData['category_ids'] = $data[0];

        $productData['name'] = trim($data[3]); /* TODO check if empty*/
        $productData['sku'] = trim($data[2]);

        $price = floatval(preg_replace("/,/", ".", trim($data[4])));
        $productData['price'] = (strlen($price) > 0) ? $price : '0.0';

        $productTypeId = trim($data[9]);

        /* Get minimal price from simple products */
        if(strlen($productTypeId) > 0 && $productTypeId == 'configurable') {
            $minPrice = $this->_getMinPrice($productData['sku']);

            if($minPrice) {
                $productData['price'] = $minPrice;
            }

        }
        /* END */

        if(strlen($productTypeId) > 0 && in_array($productTypeId, $productTypes)) {
            $productData['type_id'] = $productTypeId;
        }

        if(strlen($productTypeId) > 0 && $productTypeId !== 'simple') {
            $productData['visibility'] = Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH;
        }

        $changeLinksFromConfig = (int)Mage::getStoreConfig('wplgsynchronization/generalwplgsynchronization/change_links', Mage::app()->getStore());
        $productModel = Mage::getModel('catalog/product');
        $productId = $productModel->getIdBySku(trim($data[2]));

        if ($changeLinksFromConfig === 0 && $productId) {
            $productModel->load($productId);
            $productData['url_key'] = $productModel->getUrlKey();
        }

        return $productData;
    }

    /* Assigning associated product to configurable */
    protected function _assignSimpleToConfigurable($configurableId, $simpleIds, $groupedSku) {
        try {
            $minPrice = false;
            if(strlen($groupedSku) > 0) {
                $minPrice = $this->_getMinPrice($groupedSku);
            }

            $configurableId = (int)$configurableId;

            $attributeCvetId = '133'; //TODO 133 - id of attribute "cvet"
            $attributeCvetName = 'cvet'; //TODO 'cvet' - name of attribute "cvet"

            $configProduct = Mage::getModel('catalog/product')->load($configurableId);

            $configProduct->getTypeInstance()->setUsedProductAttributeIds(array($attributeCvetId)); //attribute ID of attribute 'color' in my store
            $configurableAttributesData = $configProduct->getTypeInstance()->getConfigurableAttributesAsArray();

            $configProduct->setCanSaveConfigurableAttributes(true);
            $configProduct->setConfigurableAttributesData($configurableAttributesData);

            /* Get associated simple products ids for configurable */
            $existSimpleIds = array();
            $configurable= Mage::getModel('catalog/product_type_configurable')->setProduct($configProduct);
            $simpleCollection = $configurable->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
            foreach($simpleCollection as $simple){
                $existSimpleIds[] = $simple->getId();
            }
            /* END */

            $configurableProductsData = array();
            foreach ($simpleIds as $simpleId) {
                if(in_array($simpleId, $existSimpleIds)) {
                    continue;
                }

                $simpleProduct = Mage::getModel('catalog/product')->load($simpleId);
                $attributeLabel = $simpleProduct->getAttributeText($attributeCvetName);

                if($minPrice) {
                    $priceForOptionVariant = floatval($simpleProduct->getFinalPrice()) - floatval($minPrice);
                }

                $priceForOptionVariant = (isset($priceForOptionVariant)) ? $priceForOptionVariant : '0';


                $productData = array(
                    'label' => $attributeLabel, //attribute label
                    'attribute_id' => $attributeCvetId, //attribute ID of attribute 'cvet' in my store
                    'value_index' => (strlen($simpleProduct->getCvet()) > 0) ? $simpleProduct->getCvet() : '', //value of 'Green' index of the attribute 'color'
                    'is_percent' => '0', //fixed/percent price for this option
                    'pricing_value' => $priceForOptionVariant, //value for the pricing
                );

                $configurableProductsData[$simpleId] = $productData;
                $configurableAttributesData[0]['values'][] = $productData;
            }

            if(!empty($configurableProductsData) && !empty($configurableAttributesData)) {
                $configProduct->setConfigurableProductsData($configurableProductsData);
                $configProduct->setConfigurableAttributesData($configurableAttributesData);

                $configProduct->save();
                $configProduct->unsetData();
            }
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
    }

    protected function _getMinPrice($groupedSku) {
        $result = false;

        $associationCollection = $this->_modelAssociation->getKeysByTypeAndValue('configurable_simple', $groupedSku);
        $simpleIdsForConfigurable = array();
        foreach ($associationCollection->getData() as $associationData) {
            $simpleIdsForConfigurable[] = $associationData['a_key'];
        }

        $simplePrices = array();
        if ($simpleIdsForConfigurable) {
            foreach ($simpleIdsForConfigurable as $simpleId) {
                $simpleProduct = Mage::getModel('catalog/product')->load($simpleId);
                $simplePrices[] = floatval($simpleProduct->getFinalPrice());
            }
        }

        if (count($simplePrices) > 0) {
            sort($simplePrices);

            $result = $simplePrices['0'];
        }

        return $result;
    }

}
