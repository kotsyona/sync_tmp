<?php

class Wplg_Synchronization_Model_Attroptions extends Mage_Core_Model_Abstract
{

    protected function findOptinInArray($options, $value) {
        $optionId = FALSE;
        foreach ($options as $option) {
            if ($option['label'] == $value) {
                $optionId = $option['value'];
                //$value_exists = true;
                break;
            }
        }

        return $optionId;
    }

    protected function _setOrAddOptionAttribute($attribute_code, $value, $label) {

        $attribute_model = Mage::getModel('eav/entity_attribute');
        $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');

        $attributeId = $attribute_model->getIdByCode('catalog_product', $attribute_code);
        $attribute = $attribute_model->load($attributeId);

        $attribute_options_model->setAttribute($attribute);
        $options = $attribute_options_model->getAllOptions(false);

        $optionId = $this->findOptinInArray($options, $value);

        /* if this option does not exist, add it. */
        if (!$optionId) {
            $attribute->setData('option', array(
                'value' => array(
                    'option' => array($value, $label)
                )
            ));
            $attribute->save();
            $optionId = $this->findOptinInArray($options, $value);
        }

        return $optionId;
    }

    /*
     * Add new attribute options
     *
     * @param int $productId
     * @param array $attributes
     */
    /*public function addattroptionsToMage($attributes) {
        if (!empty($attributes)) {
            foreach ($attributes as $attributeKey => $attributeValue) {
                if(is_array($attributeValue) && !empty($attributeValue)) {
                    foreach ($attributeValue as $attrValue) {
                        if ($attributeKey == "sex_pol") {
                            $attrLabel = ($attrValue == "Jenskaja") ? "Женская" : (($attrValue == "Mujskaja") ? "Мужская" : $attrValue);
                            $this->_setOrAddOptionAttribute($attributeKey, $attrValue, $attrLabel);
                        }
                    }
                } else {
                    if ($attributeKey == "sex_pol" && !empty($attributeValue)) {
                        $attrLabel = ($attributeValue == "Jenskaja") ? "Женская" : (($attributeValue == "Mujskaja") ? "Мужская" : $attributeValue);
                        $this->_setOrAddOptionAttribute($attributeKey, $attributeValue, $attrLabel);
                    } else {
                        $this->_setOrAddOptionAttribute($attributeKey, $attributeValue, $attributeValue);
                    }
                }
            }
        }
    }*/


 public function addattroptionsToMage($key, $label) {
        $result = false;
        $attributeCode = $this->getAttributeCodeByKeyInAssociation($key);
        if ($attributeCode) {
            $mageOptionId = $this->_setOrAddOptionAttribute($attributeCode, $label, $label);
            $result = $mageOptionId;
        }

        return $result;
    }

    public function getAttributeCodeByKeyInAssociation($key) {
        $result = false;
        $attributeId = Mage::getModel("synchronization/association")->getValueByTypeAndKeyFromAssociation('attribute', $key);
        if ($attributeId) {
            $attributeCode = Mage::getModel('eav/entity_attribute')->load($attributeId)->getAttributeCode();
            $result = $attributeCode;
        }

        return $result;
    }

    /**
     * Save product attributes options
     *
     * @param int $productId
     * @param array $attributeCodes
     * @param array $attributeValuesArray
     * @return bool or id
     *
     */
    public function saveProductAttributeOptions($productId, $attributeCodes, $attributeValuesArray) {

        $attributes = $this->_prepareAtributeOptions($attributeCodes, $attributeValuesArray);

        if (strlen($productId) > 0) {
            try {
                $product = Mage::getModel("catalog/product")->load($productId);
                foreach ($attributes as $attributeKey => $attributeValue) {
                    $sourceModel = $product->getResource()->getAttribute($attributeKey)->getSource();
                    if (is_array($attributeValue)) {
                        $product->setData($attributeKey, implode(",", $attributeValue));
                    } else {
                        $product->setData($attributeKey, $attributeValue);
                    }
                    $product->save();
                }
                return $product->getEntityId();
            } catch (Exception $ex) {
                return false;
            }
        }

        return true;

    }

    protected function _prepareAtributeOptions($attributeCodes, $attributeValuesArray) {

        $attributesToSave = array();
        foreach ($attributeCodes as $attributeKeyInAssociation) {
            $optionIds = array();
            $attributeCode = $this->getAttributeCodeByKeyInAssociation($attributeKeyInAssociation);
            $attributeValues = $attributeValuesArray[$attributeCode];
            if (strlen($attributeValues) > 0) {
                $attributeFrontendInput = Mage::getModel("synchronization/association")->getValueByTypeAndKeyFromAssociation('attribute_type', $attributeKeyInAssociation);

                switch ($attributeFrontendInput) {
                    case 'multi':
                        foreach (explode('~', $attributeValues) as $attributeOptionLabel) {
                            $optionIds[] = $this->addattroptionsToMage($attributeKeyInAssociation, $attributeOptionLabel);
                        }
                        $attributesToSave[$attributeCode] = $optionIds;
                        break;
                    case 'select':
                        foreach (explode('~', $attributeValues) as $attributeOptionLabel) {
                            $optionIds[] = $this->addattroptionsToMage($attributeKeyInAssociation, $attributeOptionLabel);
                        }
                        $attributesToSave[$attributeCode] = $optionIds;
                        break;
                    case 'string':
                        $attributesToSave[$attributeCode] = $attributeValues;
                        break;
                }
            }
        }

        return $attributesToSave;
    }

}
