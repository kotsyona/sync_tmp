<?php
class Wplg_Synchronization_Model_Category extends Mage_Core_Model_Abstract {

    public function toOptionArray() {

        $categoriesArray = array();
        $categoriesArray[0] = array(
            'label' => Mage::helper('synchronization')->__('-- Please Select a Category --'),
            'value' => 0
        );
        $_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*');
        if (count($_categories) > 0) {
            foreach ($_categories as $_category) {

                if ($_category->getEntityId() == 1)
                    continue;

                $level = '';
                for ($i = 1; $i <= $_category->getLevel(); $i++) {
                    $level .= '-';
                }

                $categoriesArray[] = array(
                    'label' => $level . $_category->getName(),
                    'value' => $_category->getEntityId(),
                );
            }
        }

        return $categoriesArray;
    }

}