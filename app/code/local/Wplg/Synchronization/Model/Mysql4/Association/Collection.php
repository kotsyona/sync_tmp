<?php

class Wplg_Synchronization_Model_Mysql4_Association_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    public function _construct() {
        $this->_init("synchronization/association");
    }

}
