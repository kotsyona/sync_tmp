<?php

class Wplg_Synchronization_Model_Mysql4_Association extends Mage_Core_Model_Mysql4_Abstract {

    protected function _construct() {
        $this->_init("synchronization/association", "id");
    }

}
