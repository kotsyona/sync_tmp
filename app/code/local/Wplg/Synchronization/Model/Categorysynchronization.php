<?php

class Wplg_Synchronization_Model_Categorysynchronization extends Mage_Core_Model_Abstract {

    protected function _construct() {

        $this->_init("synchronization/categorysynchronization");
    }

    /*
     * Get magento id category by local id category
     *
     * @param int $localIdCategory
     */
    public function getMagentoIdCategoryByLocalIdCategory($localIdCategory) {
        $collection = $this->getCollection()
                ->addFieldToSelect('id_site_category')
                ->addFieldToFilter('id_local_category', $localIdCategory);

        $category = Mage::getModel('catalog/category')->load($collection->getData('id_site_category'));
        if ($category->getId()) {
            return $collection->getData();
        } else {
            return NULL;
        }
    }

    /**
     * Prepare categories data for export
     */
    public function prepareCategoriesDataForExport() {
        $categoriesDataForExport = false;
        try {
            $categorySynchronization = $this->getCollection()
                ->addFieldToSelect('name_local_category')
                ->addFieldToSelect('id_local_category')
                ->addFieldToSelect('id_site_category');

            $siteCategoriesIds = array();

            $_categories = Mage::getModel('catalog/category')
                ->getCollection()
                ->addAttributeToSelect('*');

            if (count($_categories) > 0) {
                foreach ($_categories as $_category) {
                    if ($_category->getEntityId() == 1)
                        continue;
                    $siteCategoriesIds[] = $_category->getId();
                }
            }

            foreach ($categorySynchronization as $category) {
                $categoriesDataForExport[] = array($category->getNameLocalCategory(), $category->getIdLocalCategory(), $category->getIdSiteCategory());
                $key = array_search($category->getIdSiteCategory(), $siteCategoriesIds);
                if ($key !== false) {
                    unset($siteCategoriesIds[$key]);
                }

            }

            foreach ($siteCategoriesIds as $siteCategoryId) {
                $categoriesDataForExport[] = array('', '', $siteCategoryId);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
        return $categoriesDataForExport;
    }

}
