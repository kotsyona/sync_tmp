<?php

class Wplg_Synchronization_Model_Attribute extends Mage_Core_Model_Abstract {

    protected $_attributeData = array();
    protected $_modelAssociation;


    protected function _construct() {
        $this->_setAttributeData();
        $this->_modelAssociation = Mage::getModel("synchronization/association");
    }

    protected function _setAttributeData() {
        $this->_attributeData = array(
            'is_global' => 1,
            'default_value_text' => '',
            'default_value_yesno' => 0,
            'default_value_date' => '',
            'default_value_textarea' => '',
            'is_unique' => 0,
            'is_required' => 0,
            'is_configurable' => 0,
            'is_searchable' => 1,
            'is_visible_in_advanced_search' => 1,
            'is_comparable' => 1,
            'is_used_for_price_rules' => '',
            'is_wysiwyg_enabled' => 0,
            'is_html_allowed_on_front' => 1,
            'is_visible_on_front' => 1,
            'used_in_product_listing' => 1,
            'used_for_sort_by' => '',
            'is_filterable' => 1,
            'is_filterable_in_search' => 0,
        );
    }

    public function getAttributeData() {
        return $this->_attributeData;
    }

    /**
     * Check of existence is the attribute by Code
     */
    protected function _chkAttributeByCode($attribute_code) {
        $result = FALSE;
        $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', $attribute_code);

        if ($attribute instanceof Mage_Eav_Model_Entity_Attribute) {
            $result = $entityAttributeId = $attribute->getId();
        }

        if (empty($entityAttributeId)) {
            $result = FALSE;
        }

        return $result;
    }

    /**
     * create attribute
     */
    protected function _createAttribute($data, $attribute_set_ids) {
        $_attribute_data = $this->getAttributeData();

        try {
            $product_type = array('simple', 'grouped', 'configurable', 'virtual', 'bundle', 'downloadable');

            $group_name = "General";

            $_attribute_data['attribute_code'] = $data;
            $_attribute_data['frontend_input'] = ($data == 'sex_pol') ? 'multiselect' : 'select';
            $_attribute_data['apply_to'] = $product_type;
            $_attribute_data['frontend_label'] = $data;


            $model = Mage::getModel('catalog/resource_eav_attribute');
            if (is_null($model->getIsUserDefined()) || $model->getIsUserDefined() != 0) {
                $_attribute_data['backend_type'] = $model->getBackendTypeByInput($_attribute_data['frontend_input']);
            }
            $defaultValueField = $model->getDefaultValueByInput($_attribute_data['frontend_input']);

            $model->addData($_attribute_data);

            $model->setEntityTypeId(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId());
            $model->setIsUserDefined(1);
            $model->save();

            /* add attribute to set and group */
            $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

            /* $attribute_set_id = $setup->getAttributeSetId('catalog_product', $attribute_set_name); */
            $attribute_id = $setup->getAttributeId('catalog_product', $data['attribute_code']);
            foreach ($attribute_set_ids as $attribute_set_id) {
                $attribute_group_id = $setup->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
                $setup->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
            }

            return $attribute_id;
        } catch (Exception $e) {
            /*** echo '<p>Sorry, error occured while trying to save the attribute. Error: ' . $e->getMessage() . '</p>'; */
            return FALSE;
        }
    }

    /*
     * Add new attributes if not exist
     *
     * @param int $productId
     * @param array $attributes
     */
    public function addAttributesToMage($attributes, $attribute_set_ids) {
        if(!empty($attributes)) {
            foreach ($attributes as $attributeKey => $attributeValue) {
                $chkAttributestatus = $this->_chkAttributeByCode($attributeKey);
                if (!$chkAttributestatus) {
                    $this->_createAttribute($attributeKey, $attribute_set_ids);
                }
            }
        }

    }

    /**
     * @param $code string
     * @return string
     */
    public function getTypeAttributeByCode($code) {

        /* TODO Get it from assoc table */
        $type = 'select'; /* Dummy */

        return $type;
    }

    public function checkOrAddAttributeToMagento($label, $code, $type) {

        $result = $this->_chkAttributeByCode($code);
        if (!$result) {
            $data = array('label' => $label, 'code' => $code, 'frontend_input' => $this->getFrontendInputType($type));
            $mageAttributeId = $this->_createAttributeFromParsing($data);
            if ($mageAttributeId) {
                $result = $mageAttributeId;
            }
        }

        return $result;
    }

    /**
     * create attribute
     */
    protected function _createAttributeFromParsing($data) {
        $_attribute_data = $this->getAttributeData();
        $attribute_set_ids = array(4);

        try {

            $group_name = "General";

            $_attribute_data['attribute_code'] = $data['code'];
            $_attribute_data['frontend_input'] = $data['frontend_input'];
            $_attribute_data['frontend_label'] = $data['label'];

            $_attribute_data['backend_model'] = ($data['frontend_input'] == 'multiselect') ? 'eav/entity_attribute_backend_array' : '';

            $model = Mage::getModel('catalog/resource_eav_attribute');
            if (is_null($model->getIsUserDefined()) || $model->getIsUserDefined() != 0) {
                $_attribute_data['backend_type'] = $model->getBackendTypeByInput($_attribute_data['frontend_input']);
            }

            $model->addData($_attribute_data);

            $model->setEntityTypeId(Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId());
            $model->setIsUserDefined(1);
            $model->save();

            /* add attribute to set and group */
            $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

            $attribute_id = $setup->getAttributeId('catalog_product', $data['code']);
            foreach ($attribute_set_ids as $attribute_set_id) {
                $attribute_group_id = $setup->getAttributeGroupId('catalog_product', $attribute_set_id, $group_name);
                $setup->addAttributeToSet($entityTypeId = 'catalog_product', $attribute_set_id, $attribute_group_id, $attribute_id);
            }

            return $attribute_id;
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return FALSE;
        }
    }

    public function getFrontendInputType($type) {
        $frontendInput = '';
        switch ($type) {
            case 'multi':
                $frontendInput = 'multiselect';
                break;
            case 'string':
                $frontendInput = 'text';
                break;
            case 'select':
                $frontendInput = 'select';
                break;
        }

        return $frontendInput;
    }

    public function getFrontendInputTypeForExport($type) {
        $frontendInput = '';
        switch ($type) {
            case 'multiselect':
                $frontendInput = 'multi';
                break;
            case 'text':
                $frontendInput = 'string';
                break;
            case 'select':
                $frontendInput = 'select';
                break;
        }

        return $frontendInput;
    }

}
