<?php

class Wplg_Synchronization_Adminhtml_ProducttypesynchronizationController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu("synchronization/producttypesynchronization")->_addBreadcrumb(Mage::helper("adminhtml")->__("Producttypesynchronization  Manager"), Mage::helper("adminhtml")->__("Producttypesynchronization Manager"));
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage product types"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage product types"));
        $this->_title($this->__("Synchronization, Edit Product type"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/producttypesynchronization")->load($id);
        if ($model->getId()) {
            Mage::register("producttypesynchronization_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("synchronization/producttypesynchronization");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Manage product types"), Mage::helper("adminhtml")->__("Synchronization, Manage product types"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Product type Description"), Mage::helper("adminhtml")->__("Synchronization, Product type Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_producttypesynchronization_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_producttypesynchronization_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("synchronization")->__("Synchronization, Product type does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {

        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage product types"));
        $this->_title($this->__("Synchronization, New Product type"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/producttypesynchronization")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("producttypesynchronization_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("synchronization/producttypesynchronization");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Manage product types"), Mage::helper("adminhtml")->__("Synchronization, Manage product types"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Product type Description"), Mage::helper("adminhtml")->__("Synchronization, Product type Description"));


        $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_producttypesynchronization_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_producttypesynchronization_edit_tabs"));

        $this->renderLayout();
    }

    public function saveAction() {

        $post_data = $this->getRequest()->getPost();


        if ($post_data) {

            try {

                $model = Mage::getModel("synchronization/producttypesynchronization")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Product type was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setProducttypesynchronizationData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setProducttypesynchronizationData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("synchronization/producttypesynchronization");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Product type was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("synchronization/producttypesynchronization");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Product type(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'producttypesynchronization.csv';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_producttypesynchronization_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'producttypesynchronization.xml';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_producttypesynchronization_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
