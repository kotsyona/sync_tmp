<?php

class Wplg_Synchronization_Adminhtml_SynchronizationexportController extends Mage_Adminhtml_Controller_Action {

    public function indexAction() {
        $this->loadLayout();
        $this->_title($this->__("Synchronization, Export"));
        $this->renderLayout();
    }

    public function exportproductsAction() {
        $categoriesIds = $this->getRequest()->getParam('wplg_synchronization_export_products_select');

        $result = Mage::getModel("synchronization/export")->exportProductsToFile($categoriesIds);

        echo $result;
    }

    public function exportcategoriesAction() {
        $result = Mage::getModel("synchronization/export")->exportCategoriesToFile();

        echo $result;
    }

    public function exportproducttypesAction() {
        $result = Mage::getModel("synchronization/export")->exportProductTypesToFile();

        echo $result;
    }
}
