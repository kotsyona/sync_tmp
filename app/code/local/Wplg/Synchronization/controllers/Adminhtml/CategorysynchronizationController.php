<?php

class Wplg_Synchronization_Adminhtml_CategorysynchronizationController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu("synchronization/categorysynchronization")->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Manage Categories"), Mage::helper("adminhtml")->__("Synchronization, Manage Categories"));
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage Categories"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage Categories"));
        $this->_title($this->__("Synchronization, Edit category"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/categorysynchronization")->load($id);
        if ($model->getId()) {
            Mage::register("categorysynchronization_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("synchronization/categorysynchronization");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Manage Categories"), Mage::helper("adminhtml")->__("Synchronization, Manage Categories"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Category Description"), Mage::helper("adminhtml")->__("Synchronization, Category Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_categorysynchronization_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_categorysynchronization_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("synchronization")->__("Synchronization, Category does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {

        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manage Categories"));
        $this->_title($this->__("New Category"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/categorysynchronization")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("categorysynchronization_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("synchronization/categorysynchronization");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Manage Categories"), Mage::helper("adminhtml")->__("Synchronization, Manage Categories"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Category Description"), Mage::helper("adminhtml")->__("Synchronization, Category Description"));


        $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_categorysynchronization_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_categorysynchronization_edit_tabs"));

        $this->renderLayout();
    }

    public function saveAction() {

        $post_data = $this->getRequest()->getPost();


        if ($post_data) {

            try {



                $model = Mage::getModel("synchronization/categorysynchronization")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Category was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setCategorysynchronizationData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setCategorysynchronizationData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("synchronization/categorysynchronization");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Category was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("synchronization/categorysynchronization");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Synchronization, Category(-s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'categorysynchronization.csv';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_categorysynchronization_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'categorysynchronization.xml';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_categorysynchronization_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
