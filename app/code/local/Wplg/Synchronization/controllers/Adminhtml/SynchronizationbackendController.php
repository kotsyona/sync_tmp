<?php

class Wplg_Synchronization_Adminhtml_SynchronizationbackendController extends Mage_Adminhtml_Controller_Action {


    public function indexxAction() {

        $text = '{"progressStatus":"continue","id":"1","filename":"2014-10-06_19-17-04_original-parphum.csv","position":"5","total_rows":"61","status":"0","options":null}';

        $modelFilesToImport = Mage::getModel("synchronization/filestoimport");
        $file = $modelFilesToImport->getFileToImport();
        $id = $file->getId();

        $filesToImport = $modelFilesToImport->load($id)->setOptions($text)->save();

        echo '<pre>';
        print_r($filesToImport->getData());
        echo '</pre>';

    }

    public function indexAction() {
        $this->loadLayout();
        $this->_title($this->__("Synchronization, Import"));
        $this->renderLayout();
    }

    public function uploadImportCSVAction() {
        $fname = '';
        $importCSV = '';

        $type = (string)trim($this->getRequest()->getParam('type'));




        if($type != '') {
            switch ($type) {
                case "category":
                    $importCSV = 'importCategoryCSV';
                    break;
                case "productType":
                    $importCSV = 'importProductTypeCSV';
                    break;
                case "importCSV":
                    $importCSV = 'importCSV';
                    break;
            }
        }



        $helper = Mage::helper('synchronization');

        if (isset($_FILES[$importCSV]['name']) && $_FILES[$importCSV]['name'] != '') {
            try {
                $path = $helper->getSyncPath('files');  //destination directory + '/files/'
                $fname = date('Y-m-d_H-i-s', Mage::getModel('core/date')->timestamp(time())) . '_' .  $_FILES[$importCSV]['name']; //file name
                $fname = preg_replace('/[^A-Za-z0-9_.-]/', '', $fname);
                $uploader = new Varien_File_Uploader($importCSV); //load class
                $uploader->setAllowedExtensions(array('csv')); //Allowed extension for file
                $uploader->setAllowCreateFolders(true); //for creating the directory if not exists
                $uploader->setAllowRenameFiles(false); //if true, uploaded file's name will be changed, if file with the same name already exists directory.
                $uploader->setFilesDispersion(false);
                $uploader->save($path, $fname); //save the file on the specified path

                if ($importCSV == 'importCSV') {
                    Mage::getModel("synchronization/filestoimport")->parseHeaderCsv($path, $fname);
                }

            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/");
                return;
            }

        } else {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Your file is not loaded.'));
        }

        if (strlen($fname) > 0) {
            if ($importCSV == 'importCategoryCSV')
                $this->_forward('parseCategoryCSV');
            else if ($importCSV == 'importProductTypeCSV')
                $this->_forward('parseProductTypeCSV');
        }

        $this->_redirect("*/*/");
        return;
    }

    public function processProgressAction() {
        $modelFilestoimport = Mage::getModel("synchronization/filestoimport");
        $file = $modelFilestoimport->getFileToImport();

        echo json_encode($file->getData());
    }

    public function processAction() {
        $modelAdapter = Mage::getModel("synchronization/adapter");
        /* $modelAdapter = Mage::getModel("synchronization/" . Mage::helper('synchronization')->getAdapterName()); */


        if ($modelAdapter->parseFile()) {
            $modelFilestoimport = Mage::getModel("synchronization/filestoimport");

            /***$result = array('id' => $id, 'filename' => $filename, 'position' => $position, 'total_rows' => $totalRows, 'status' => $status, 'options' => $options);*/
            echo $modelFilestoimport->processStatus();
        }
    }

    public function parseCategoryCSVAction() {

        $importCategories = array();

        $helper = Mage::helper('synchronization');
        $csvKeys = array('name_local_category', 'id_local_category', 'id_site_category');
        $delimiter = ",";

        $path = $helper->getSyncPath('files');
        $file = $helper->getCSVFileToParse($path);

        if ($file) {
            $messages = array();
            $parsedData = $helper->csv_to_array($path . $file, $delimiter, $csvKeys);

            $modelCategorysynchronization = Mage::getModel("synchronization/Categorysynchronization");

            if ($parsedData) {
                foreach ($parsedData as $data) {
                    $importCategories['name_local_category'] = trim($data['name_local_category']);
                    $importCategories['id_local_category'] = trim($data['id_local_category']);
                    $importCategories['id_site_category'] = trim($data['id_site_category']);

                    $category = Mage::getModel('catalog/category')->load($importCategories['id_site_category']);
                    if ($category->getId()) {
                        try {
                            if ($modelCategorysynchronization->load($importCategories['id_local_category'], 'id_local_category')->getIdLocalCategory() == $importCategories['id_local_category']) {
                                $modelCategorysynchronization->addData($importCategories)->save();
                            } else {
                                $modelCategorysynchronization->setData($importCategories)->save();
                            }
                            $messages[] = $this->__("Category with id - %s was successfully imported.", $importCategories['id_site_category']);
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        }
                    } else {
                        try {
                            if ($modelCategorysynchronization->load($importCategories['id_local_category'], 'id_local_category')->getIdLocalCategory() == $importCategories['id_local_category']) {
                                $modelCategorysynchronization->load($importCategories['id_local_category'], 'id_local_category')->addData($importCategories)->setIdSiteCategory('0')->save();
                            } else {
                                $modelCategorysynchronization->load($importCategories['id_local_category'], 'id_local_category')->setData($importCategories)->setIdSiteCategory('0')->save();
                            }
                            $messages[] = $this->__("Category with id - %s not found.", $importCategories['id_site_category']);
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        }
                    }
                }
                Mage::getSingleton('core/session')->setMessageImportCategories(serialize($messages));
            } else {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while importing categories.'));
            }
            unlink($path . $file);
        }
    }

    public function parseProductTypeCSVAction() {

        $importProductTypes = array();

        $helper = Mage::helper('synchronization');
        $csvKeys = array('name_local_product_type', 'id_local_product_type', 'id_site_product_type');
        $delimiter = ",";

        $path = $helper->getSyncPath('files');
        $file = $helper->getCSVFileToParse($path);

        if ($file) {
            $messages = array();
            $parsedData = $helper->csv_to_array($path . $file, $delimiter, $csvKeys);

            $modelProducttype = Mage::getModel("synchronization/Producttypesynchronization");

            if ($parsedData) {
                foreach ($parsedData as $data) {
                    $importProductTypes['name_local_product_type'] = trim($data['name_local_product_type']);
                    $importProductTypes['id_local_product_type'] = trim($data['id_local_product_type']);
                    $importProductTypes['id_site_product_type'] = trim($data['id_site_product_type']);

                    $attribute_set = Mage::getModel("eav/entity_attribute_set")->getCollection()
                            ->addFieldToSelect('*')
                            ->addFieldToFilter("attribute_set_id", $importProductTypes['id_site_product_type']);

                if ($attribute_set->getData()) {
                        try {
                            if ($modelProducttype->load($importProductTypes['id_local_product_type'], 'id_local_product_type')->getIdLocalProductType() == $importProductTypes['id_local_product_type']) {
                                $modelProducttype->addData($importProductTypes)->save();
                            } else {
                                $modelProducttype->setData($importProductTypes)->save();
                            }
                            $messages[] = $this->__("Attribute set with id - %s was successfully imported.", $importProductTypes['id_site_product_type']);
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        }
                    } else {
                        try {
                            if ($modelProducttype->load($importProductTypes['id_local_product_type'], 'id_local_product_type')->getIdLocalProductType() == $importProductTypes['id_local_product_type']) {
                                $modelProducttype->load($importProductTypes['id_local_product_type'], 'id_local_product_type')->addData($importProductTypes)->setIdSiteProductType('0')->save();
                            } else {
                                $modelProducttype->load($importProductTypes['id_local_product_type'], 'id_local_product_type')->setData($importProductTypes)->setIdSiteProductType('0')->save();
                            }
                            $messages[] = $this->__("Attribute set with id - %s not found.", $importProductTypes['id_site_product_type']);
                        } catch (Exception $e) {
                            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                        }
                    }
                }
                Mage::getSingleton('core/session')->setMessageImportProductTypes(serialize($messages));
            } else {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while importing product types.'));
            }
            unlink($path . $file);
        }
    }

}
