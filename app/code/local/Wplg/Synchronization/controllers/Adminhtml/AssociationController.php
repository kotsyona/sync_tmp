<?php

class Wplg_Synchronization_Adminhtml_AssociationController extends Mage_Adminhtml_Controller_Action {

    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu("synchronization/association")->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Association  Manager"), Mage::helper("adminhtml")->__("Synchronization, Association Manager"));
        return $this;
    }

    public function indexAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Synchronization, Manager Association"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction() {
        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Association"));
        $this->_title($this->__("Synchronization, Edit Association"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/association")->load($id);
        if ($model->getId()) {
            Mage::register("association_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("synchronization/association");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Association Manager"), Mage::helper("adminhtml")->__("Synchronization, Association Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Association Description"), Mage::helper("adminhtml")->__("Synchronization, Association Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_association_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_association_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("synchronization")->__("Association does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function newAction() {

        $this->_title($this->__("Synchronization"));
        $this->_title($this->__("Association"));
        $this->_title($this->__("Synchronization, New Association"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("synchronization/association")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("association_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("synchronization/association");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Association Manager"), Mage::helper("adminhtml")->__("Synchronization, Association Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Synchronization, Association Description"), Mage::helper("adminhtml")->__("Synchronization, Association Description"));


        $this->_addContent($this->getLayout()->createBlock("synchronization/adminhtml_association_edit"))->_addLeft($this->getLayout()->createBlock("synchronization/adminhtml_association_edit_tabs"));

        $this->renderLayout();
    }

    public function saveAction() {

        $post_data = $this->getRequest()->getPost();


        if ($post_data) {

            try {
                $model = Mage::getModel("synchronization/association")
                        ->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Association was successfully saved"));
                Mage::getSingleton("adminhtml/session")->setAssociationData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setAssociationData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("synchronization/association");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Association was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("synchronization/association");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Association(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'association.csv';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_association_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'association.xml';
        $grid = $this->getLayout()->createBlock('synchronization/adminhtml_association_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
