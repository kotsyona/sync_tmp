<?php

class Wplg_Synchronization_Block_Adminhtml_Categorysynchronization_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "synchronization";
        $this->_controller = "adminhtml_categorysynchronization";
        $this->_updateButton("save", "label", Mage::helper("synchronization")->__("Synchronization, Save category"));
        $this->_updateButton("delete", "label", Mage::helper("synchronization")->__("Synchronization, Delete category"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("synchronization")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);



        $this->_formScripts[] = "function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
            }";
    }

    public function getHeaderText() {
        if (Mage::registry("categorysynchronization_data") && Mage::registry("categorysynchronization_data")->getId()) {

            return Mage::helper("synchronization")->__("Synchronization, Edit category '%s'", $this->htmlEscape(Mage::registry("categorysynchronization_data")->getId()));
        } else {

            return Mage::helper("synchronization")->__("Synchronization, Add category");
        }
    }

}
