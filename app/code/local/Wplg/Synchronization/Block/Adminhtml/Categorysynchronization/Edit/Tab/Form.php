<?php

class Wplg_Synchronization_Block_Adminhtml_Categorysynchronization_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("synchronization_form", array("legend" => Mage::helper("synchronization")->__("Synchronization, Category information")));


        /*         * * $fieldset->addField("id", "text", array(
          "label" => Mage::helper("synchronization")->__("Id"),
          "name" => "id",
          )); */

        $fieldset->addField("name_local_category", "text", array(
            "label" => Mage::helper("synchronization")->__("Name local category"),
            "name" => "name_local_category",
        ));

        $fieldset->addField("id_local_category", "text", array(
            "label" => Mage::helper("synchronization")->__("Id local category"),
            "name" => "id_local_category",
        ));

        /*** $fieldset->addField("id_site_category", "text", array(
            "label" => Mage::helper("synchronization")->__("Id site category"),
            "name" => "id_site_category",
        )); */

        $fieldset->addField('id_site_category', 'select', array(
            'label' => Mage::helper('synchronization')->__('Id site category'),
            'name' => 'id_site_category',
            'value' => '0',
            'values' => Mage::getModel('synchronization/category')->toOptionArray(),
        ));

        if (Mage::getSingleton("adminhtml/session")->getCategorysynchronizationData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getCategorysynchronizationData());
            Mage::getSingleton("adminhtml/session")->setCategorysynchronizationData(null);
        } elseif (Mage::registry("categorysynchronization_data")) {
            $form->setValues(Mage::registry("categorysynchronization_data")->getData());
        }
        return parent::_prepareForm();
    }

}
