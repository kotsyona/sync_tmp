<?php

class Wplg_Synchronization_Block_Adminhtml_Categorysynchronization_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("categorysynchronizationGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("synchronization/categorysynchronization")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn("id", array(
            "header" => Mage::helper("synchronization")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "id",
        ));

        $this->addColumn("name_local_category", array(
            "header" => Mage::helper("synchronization")->__("Name local category"),
            "index" => "name_local_category",
        ));
        $this->addColumn("id_local_category", array(
            "header" => Mage::helper("synchronization")->__("Id local category"),
            "index" => "id_local_category",
        ));
        $this->addColumn("id_site_category", array(
            "header" => Mage::helper("synchronization")->__("Id site category"),
            "index" => "id_site_category",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_categorysynchronization', array(
            'label' => Mage::helper('synchronization')->__('Remove'),
            'url' => $this->getUrl('*/adminhtml_categorysynchronization/massRemove'),
            'confirm' => Mage::helper('synchronization')->__('Are you sure?')
        ));
        return $this;
    }

}
