<?php

class Wplg_Synchronization_Block_Adminhtml_Producttypesynchronization_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("producttypesynchronizationGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("synchronization/producttypesynchronization")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn("id", array(
            "header" => Mage::helper("synchronization")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "id",
        ));

        $this->addColumn("name_local_product_type", array(
            "header" => Mage::helper("synchronization")->__("Name local product type"),
            "index" => "name_local_product_type",
        ));
        $this->addColumn("id_local_product_type", array(
            "header" => Mage::helper("synchronization")->__("Id local product type"),
            "index" => "id_local_product_type",
        ));
        $this->addColumn("id_site_product_type", array(
            "header" => Mage::helper("synchronization")->__("Id site product type"),
            "index" => "id_site_product_type",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_producttypesynchronization', array(
            'label' => Mage::helper('synchronization')->__('Remove'),
            'url' => $this->getUrl('*/adminhtml_producttypesynchronization/massRemove'),
            'confirm' => Mage::helper('synchronization')->__('Are you sure?')
        ));
        return $this;
    }

}
