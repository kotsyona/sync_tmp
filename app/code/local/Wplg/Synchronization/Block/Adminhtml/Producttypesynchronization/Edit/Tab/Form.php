<?php

class Wplg_Synchronization_Block_Adminhtml_Producttypesynchronization_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("synchronization_form", array("legend" => Mage::helper("synchronization")->__("Synchronization, Product type information")));


        /*** $fieldset->addField("id", "text", array(
            "label" => Mage::helper("synchronization")->__("Id"),
            "name" => "id",
        )); */

        $fieldset->addField("name_local_product_type", "text", array(
            "label" => Mage::helper("synchronization")->__("Name local product type"),
            "name" => "name_local_product_type",
        ));

        $fieldset->addField("id_local_product_type", "text", array(
            "label" => Mage::helper("synchronization")->__("Id local product type"),
            "name" => "id_local_product_type",
        ));

        /*** $fieldset->addField("id_site_product_type", "text", array(
            "label" => Mage::helper("synchronization")->__("Id site product type"),
            "name" => "id_site_product_type",
        )); */

        $fieldset->addField('id_site_product_type', 'select', array(
            'label' => Mage::helper('synchronization')->__('Id site product type'),
            'name' => 'id_site_product_type',
            'value' => '0',
            'values' => Mage::getModel('synchronization/attributeset')->toOptionArray(),
        ));


        if (Mage::getSingleton("adminhtml/session")->getProducttypesynchronizationData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getProducttypesynchronizationData());
            Mage::getSingleton("adminhtml/session")->setProducttypesynchronizationData(null);
        } elseif (Mage::registry("producttypesynchronization_data")) {
            $form->setValues(Mage::registry("producttypesynchronization_data")->getData());
        }
        return parent::_prepareForm();
    }

}
