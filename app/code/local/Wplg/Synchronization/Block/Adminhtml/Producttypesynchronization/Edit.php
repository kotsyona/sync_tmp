<?php

class Wplg_Synchronization_Block_Adminhtml_Producttypesynchronization_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "synchronization";
        $this->_controller = "adminhtml_producttypesynchronization";
        $this->_updateButton("save", "label", Mage::helper("synchronization")->__("Synchronization, Save Product type"));
        $this->_updateButton("delete", "label", Mage::helper("synchronization")->__("Synchronization, Delete Product type"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("synchronization")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);



        $this->_formScripts[] = "function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
            }
            ";
    }

    public function getHeaderText() {
        if (Mage::registry("producttypesynchronization_data") && Mage::registry("producttypesynchronization_data")->getId()) {

            return Mage::helper("synchronization")->__("Synchronization, Edit Product type '%s'", $this->htmlEscape(Mage::registry("producttypesynchronization_data")->getId()));
        } else {

            return Mage::helper("synchronization")->__("Synchronization, Add Product type");
        }
    }

}
