<?php

class Wplg_Synchronization_Block_Adminhtml_Producttypesynchronization_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("producttypesynchronization_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("synchronization")->__("Synchronization, Product type information"));
    }

    protected function _beforeToHtml() {
        $this->addTab("form_section", array(
            "label" => Mage::helper("synchronization")->__("Synchronization, Product type information"),
            "title" => Mage::helper("synchronization")->__("Synchronization, Product type information"),
            "content" => $this->getLayout()->createBlock("synchronization/adminhtml_producttypesynchronization_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
