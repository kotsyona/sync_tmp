<?php
class Wplg_Synchronization_Block_Adminhtml_Filestoimport_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("filestoimport_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("synchronization")->__("Item Information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("synchronization")->__("Item Information"),
				"title" => Mage::helper("synchronization")->__("Item Information"),
				"content" => $this->getLayout()->createBlock("synchronization/adminhtml_filestoimport_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
