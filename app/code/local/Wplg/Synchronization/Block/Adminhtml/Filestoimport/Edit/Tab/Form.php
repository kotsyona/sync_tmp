<?php
class Wplg_Synchronization_Block_Adminhtml_Filestoimport_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
				$this->setForm($form);
				$fieldset = $form->addFieldset("synchronization_form", array("legend"=>Mage::helper("synchronization")->__("Item information")));


						/*$fieldset->addField("id", "text", array(
						"label" => Mage::helper("synchronization")->__("Id"),
						"name" => "id",
						));*/

						$fieldset->addField("filename", "text", array(
						"label" => Mage::helper("synchronization")->__("Filename"),
						"name" => "filename",
						));

						$fieldset->addField("position", "text", array(
						"label" => Mage::helper("synchronization")->__("Position"),
						"name" => "position",
						));

                                                $fieldset->addField("total_rows", "text", array(
						"label" => Mage::helper("synchronization")->__("Total rows"),
						"name" => "total_rows",
						));

                                                $fieldset->addField("options", "text", array(
						"label" => Mage::helper("synchronization")->__("Options"),
						"name" => "options",
						));

                                                $fieldset->addField("status", "text", array(
						"label" => Mage::helper("synchronization")->__("Status"),
						"name" => "status",
						));

                                                $fieldset->addField("attribute_codes", "text", array(
						"label" => Mage::helper("synchronization")->__("Attribute codes"),
						"name" => "attribute_codes",
						));



				if (Mage::getSingleton("adminhtml/session")->getFilestoimportData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getFilestoimportData());
					Mage::getSingleton("adminhtml/session")->setFilestoimportData(null);
				}
				elseif(Mage::registry("filestoimport_data")) {
				    $form->setValues(Mage::registry("filestoimport_data")->getData());
				}
				return parent::_prepareForm();
		}
}
