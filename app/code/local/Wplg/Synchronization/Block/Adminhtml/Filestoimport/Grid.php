<?php

class Wplg_Synchronization_Block_Adminhtml_Filestoimport_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("filestoimportGrid");
				$this->setDefaultSort("id");
				$this->setDefaultDir("DESC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("synchronization/filestoimport")->getCollection();
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("id", array(
				"header" => Mage::helper("synchronization")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "id",
				));

				$this->addColumn("filename", array(
				"header" => Mage::helper("synchronization")->__("Filename"),
				"index" => "filename",
				));
				$this->addColumn("position", array(
				"header" => Mage::helper("synchronization")->__("Position"),
				"index" => "position",
				));
                                $this->addColumn("total_rows", array(
				"header" => Mage::helper("synchronization")->__("Total rows"),
				"index" => "total_rows",
				));
                                $this->addColumn("options", array(
				"header" => Mage::helper("synchronization")->__("Options"),
				"index" => "options",
				));
				$this->addColumn("status", array(
				"header" => Mage::helper("synchronization")->__("Status"),
				"index" => "status",
				));
				$this->addColumn("attribute_codes", array(
				"header" => Mage::helper("synchronization")->__("Attribute codes"),
				"index" => "attribute_codes",
				));
			$this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
			$this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}



		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('id');
			$this->getMassactionBlock()->setFormFieldName('ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_filestoimport', array(
					 'label'=> Mage::helper('synchronization')->__('Remove Filestoimport'),
					 'url'  => $this->getUrl('*/adminhtml_filestoimport/massRemove'),
					 'confirm' => Mage::helper('synchronization')->__('Are you sure?')
				));
			return $this;
		}


}