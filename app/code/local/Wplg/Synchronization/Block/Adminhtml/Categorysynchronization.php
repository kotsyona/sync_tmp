<?php

class Wplg_Synchronization_Block_Adminhtml_Categorysynchronization extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = "adminhtml_categorysynchronization";
        $this->_blockGroup = "synchronization";
        $this->_headerText = Mage::helper("synchronization")->__("Synchronization, Manage Categories");
        $this->_addButtonLabel = Mage::helper("synchronization")->__("Synchronization, Add New Category");
        parent::__construct();
    }

}
