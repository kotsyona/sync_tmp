<?php


class Wplg_Synchronization_Block_Adminhtml_Filestoimport extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_filestoimport";
	$this->_blockGroup = "synchronization";
	$this->_headerText = Mage::helper("synchronization")->__("Filestoimport Manager");
	$this->_addButtonLabel = Mage::helper("synchronization")->__("Add New Item");
	parent::__construct();
	
	}

}