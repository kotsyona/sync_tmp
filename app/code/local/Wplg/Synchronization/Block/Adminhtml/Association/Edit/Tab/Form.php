<?php

class Wplg_Synchronization_Block_Adminhtml_Association_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("synchronization_form", array("legend" => Mage::helper("synchronization")->__("Synchronization, Association information")));


        $fieldset->addField("a_type", "text", array(
            "label" => Mage::helper("synchronization")->__("Type"),
            "name" => "a_type",
        ));

        $fieldset->addField("a_key", "text", array(
            "label" => Mage::helper("synchronization")->__("Key"),
            "name" => "a_key",
        ));

        $fieldset->addField("a_value", "text", array(
            "label" => Mage::helper("synchronization")->__("Value"),
            "name" => "a_value",
        ));


        if (Mage::getSingleton("adminhtml/session")->getAssociationData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getAssociationData());
            Mage::getSingleton("adminhtml/session")->setAssociationData(null);
        } elseif (Mage::registry("association_data")) {
            $form->setValues(Mage::registry("association_data")->getData());
        }
        return parent::_prepareForm();
    }

}
