<?php

class Wplg_Synchronization_Block_Adminhtml_Association_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId("associationGrid");
        $this->setDefaultSort("id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getModel("synchronization/association")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn("id", array(
            "header" => Mage::helper("synchronization")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "id",
        ));

        $this->addColumn("a_type", array(
            "header" => Mage::helper("synchronization")->__("Type"),
            "index" => "a_type",
        ));
        $this->addColumn("a_key", array(
            "header" => Mage::helper("synchronization")->__("Key"),
            "index" => "a_key",
        ));
        $this->addColumn("a_value", array(
            "header" => Mage::helper("synchronization")->__("Value"),
            "index" => "a_value",
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row) {
        return $this->getUrl("*/*/edit", array("id" => $row->getId()));
    }

    protected function _prepareMassaction() {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_association', array(
            'label' => Mage::helper('synchronization')->__('Remove'),
            'url' => $this->getUrl('*/adminhtml_association/massRemove'),
            'confirm' => Mage::helper('synchronization')->__('Are you sure?')
        ));
        return $this;
    }

}
