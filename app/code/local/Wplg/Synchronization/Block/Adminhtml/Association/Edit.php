<?php

class Wplg_Synchronization_Block_Adminhtml_Association_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();
        $this->_objectId = "id";
        $this->_blockGroup = "synchronization";
        $this->_controller = "adminhtml_association";
        $this->_updateButton("save", "label", Mage::helper("synchronization")->__("Synchronization, Save Association"));
        $this->_updateButton("delete", "label", Mage::helper("synchronization")->__("Synchronization, Delete Association"));

        $this->_addButton("saveandcontinue", array(
            "label" => Mage::helper("synchronization")->__("Save And Continue Edit"),
            "onclick" => "saveAndContinueEdit()",
            "class" => "save",
                ), -100);



        $this->_formScripts[] = "function saveAndContinueEdit(){
            editForm.submit($('edit_form').action+'back/edit/');
            }";
    }

    public function getHeaderText() {
        if (Mage::registry("association_data") && Mage::registry("association_data")->getId()) {

            return Mage::helper("synchronization")->__("Synchronization, Edit Association '%s'", $this->htmlEscape(Mage::registry("association_data")->getId()));
        } else {

            return Mage::helper("synchronization")->__("Synchronization, Add Association");
        }
    }

}
