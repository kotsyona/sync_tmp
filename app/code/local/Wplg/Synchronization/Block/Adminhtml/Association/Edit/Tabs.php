<?php

class Wplg_Synchronization_Block_Adminhtml_Association_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {
        parent::__construct();
        $this->setId("association_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("synchronization")->__("Synchronization, Association Information"));
    }

    protected function _beforeToHtml() {
        $this->addTab("form_section", array(
            "label" => Mage::helper("synchronization")->__("Synchronization, Association Information"),
            "title" => Mage::helper("synchronization")->__("Synchronization, Association Information"),
            "content" => $this->getLayout()->createBlock("synchronization/adminhtml_association_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
