<?php

class Wplg_Synchronization_Block_Adminhtml_Producttypesynchronization extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = "adminhtml_producttypesynchronization";
        $this->_blockGroup = "synchronization";
        $this->_headerText = Mage::helper("synchronization")->__("Synchronization, Manage product types");
        $this->_addButtonLabel = Mage::helper("synchronization")->__("Synchronization, Add New Product types");
        parent::__construct();
    }

}
