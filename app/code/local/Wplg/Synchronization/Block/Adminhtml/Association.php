<?php

class Wplg_Synchronization_Block_Adminhtml_Association extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {

        $this->_controller = "adminhtml_association";
        $this->_blockGroup = "synchronization";
        $this->_headerText = Mage::helper("synchronization")->__("Synchronization, Association Manager");
        $this->_addButtonLabel = Mage::helper("synchronization")->__("Synchronization, Add New Association");
        parent::__construct();
    }

}
