<?php

$installer = $this;
$installer->startSetup();

$sql = <<<SQLTEXT
DROP TABLE IF EXISTS `wplg_synchronization_association`;
CREATE TABLE IF NOT EXISTS `wplg_synchronization_association` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `a_type` varchar(255) DEFAULT NULL,
  `a_key` varchar(255) DEFAULT NULL,
  `a_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();