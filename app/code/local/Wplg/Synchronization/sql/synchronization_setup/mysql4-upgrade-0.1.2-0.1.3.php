<?php

$installer = $this;
$installer->startSetup();

$sql = <<<SQLTEXT
ALTER TABLE `wplg_synchronization_files_to_import` ADD `attribute_codes` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL ;
SQLTEXT;

$installer->run($sql);
$installer->endSetup();