<?php

$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
DROP TABLE IF EXISTS `wplg_synchronization_category`;
CREATE TABLE IF NOT EXISTS `wplg_synchronization_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_local_category` varchar(255) NOT NULL default '',
  `id_local_category` int(11) NOT NULL,  
  `id_site_category` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `wplg_synchronization_product_type`;
CREATE TABLE IF NOT EXISTS `wplg_synchronization_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_local_product_type` varchar(255) NOT NULL default '',
  `id_local_product_type` int(11) NOT NULL,  
  `id_site_product_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
SQLTEXT;

$installer->run($sql);
//demo 
//Mage::getModel('core/url_rewrite')->setId(null);
//demo 
$installer->endSetup();
