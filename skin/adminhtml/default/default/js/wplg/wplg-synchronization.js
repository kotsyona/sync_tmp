var $j = jQuery.noConflict();
$j(document).ready(function () {
    
    /* hide/show texarea on synchronizationbackend.phtml */
    ($j("#wplg-synchronization-import-products-textarea").is(':empty')) ? ($j("#wplg-synchronization-import-products-textarea").css("display","none")) : ($j("#wplg-synchronization-import-products-textarea").css("display","block"));
    ($j("#wplg-synchronization-import-categories-textarea").is(':empty')) ? ($j("#wplg-synchronization-import-categories-textarea").css("display","none")) : ($j("#wplg-synchronization-import-categories-textarea").css("display","block"));
    ($j("#wplg-synchronization-import-product-types-textarea").is(':empty')) ? ($j("#wplg-synchronization-import-product-types-textarea").css("display","none")) : ($j("#wplg-synchronization-import-product-types-textarea").css("display","block"));

    checkIfFilesExistInTable();

    $j('#wplg_synchronization_btn_process').click(function () {
        $j(this).attr("disabled", true).css("opacity", "0.5");
        ajaxProcessProgress();

        setTimeout(function () {
            ajaxProcess();
        }, 1000);
    });

    var ajaxProcessProgress = function () {

        $j.ajax({
            type: "GET",
            url: $j('#process_progress').val(),
            success: function (data) {
                console.log(data);

                var result = $j.parseJSON(data);
                setProcessProgress(result);
            }
        });
    };

    var ajaxProcess = function () {        
        $j.ajax({
            type: "GET",
            url: $j('#process').val(),
            success: function (data) {                
                console.log(data);

                var result = $j.parseJSON(data);
                setProcessProgress(result);
                
                setPositionOfProccesFileInTable(result.id, result.position, result.total_rows);
                checkIfFilesExistInTable();
                
                if (parseInt(result.position) <= parseInt(result.total_rows) && result.progressStatus !== 'end') {
                    /*** console.log(result.position); */
                    ajaxProcess();                    
                } else {
                    $j('#wplg_synchronization_btn_process').attr("disabled", false).css("opacity", "1");
                    $j("#wplg-synchronization-import-products-textarea").css("display","none");
                }
            }
        });
    };

    var setProcessProgress = function (result) {
        var filename = result.filename;
        var totalRows = result.total_rows;
        var position = result.position;
        tblToGrabProgress = position * 100 / totalRows;
        tblToGrabProgressStep = 100 / totalRows;
        $j("#tbl_progress").css("width", tblToGrabProgress.toFixed() + "%");
        $j("#tbl_progress_text").html(filename + " - " + tblToGrabProgress.toFixed() + "%");
    };

    var setPositionOfProccesFileInTable = function (id, position) {
        $j("#wplg-synchronization-file-" + id + "-position").text(position);
        
        var str = $j('table#wplg-synchronization-table-files-to-import tr:eq(1) td:eq(0)').attr('id');        
        var processingId = str.replace("wplg-synchronization-file-", "");        
        if (parseInt(processingId) !== parseInt(id) || id === '') {
            $j('table#wplg-synchronization-table-files-to-import tr:eq(1)').remove();
        }
    };


    /* Export products */
    /* Hide result div if empty */
    if($j.trim($j("#wplg_synchronization_export_products_result").html())=='') {
        $j("#wplg_synchronization_export_products_result").css('display', 'none');
    }

    /* Hide message textarea if empty */
    if($j.trim($j("#wplg_synchronization_export_products_textarea").html())=='') {
        $j("#wplg_synchronization_export_products_textarea").css('display', 'none');
    }

    /* Disable product export button */
    $j('#wplg_synchronization_export_products_btn').attr("disabled", true).css("opacity", "0.5");

    /* Enable/disable product export button if category selected */
    $j("#wplg_synchronization_export_products_select").on('change', function (e) {
        if(this.value.length > 0) {
            $j('#wplg_synchronization_export_products_btn').attr('disabled', false).css("opacity", "1");
        } else {
            $j('#wplg_synchronization_export_products_btn').attr("disabled", true).css("opacity", "0.5");
        }
    });

    /* Generate product export files */
    $j('#wplg_synchronization_export_products_btn').click(function () {
        $j("#wplg_synchronization_export_products_result").html('').css('display', 'none');
        $j("#wplg_synchronization_export_products_textarea").html('').css('display', 'none');
        doExportProducts();
    });
    /* END */


    /* Export categories */
    /* Hide result div if empty */
    if($j.trim($j("#wplg_synchronization_export_categories_result").html())=='') {
        $j("#wplg_synchronization_export_categories_result").css('display', 'none');
    }

    /* Hide message textarea if empty */
    if($j.trim($j("#wplg_synchronization_export_categories_textarea").html())=='') {
        $j("#wplg_synchronization_export_categories_textarea").css('display', 'none');
    }

    /* Generate product export files */
    $j('#wplg_synchronization_export_categories_btn').click(function () {
        $j("#wplg_synchronization_export_categories_result").html('').css('display', 'none');
        $j("#wplg_synchronization_export_categories_textarea").html('').css('display', 'none');
        doExportCategories();
    });
    /* END */

    /* Export product types */
    /* Hide result div if empty */
    if($j.trim($j("#wplg_synchronization_export_product_types_result").html())=='') {
        $j("#wplg_synchronization_export_product_types_result").css('display', 'none');
    }

    /* Hide message textarea if empty */
    if($j.trim($j("#wplg_synchronization_export_product_types_textarea").html())=='') {
        $j("#wplg_synchronization_export_product_types_textarea").css('display', 'none');
    }

    /* Generate product export files */
    $j('#wplg_synchronization_export_product_types_btn').click(function () {
        $j("#wplg_synchronization_export_product_types_result").html('').css('display', 'none');
        $j("#wplg_synchronization_export_product_types_textarea").html('').css('display', 'none');
        doExportProductTypes();
    });
    /* END */

});

function checkIfFilesExistInTable() {
    var rowCount = $j('table#wplg-synchronization-table-files-to-import tr:last').index() + 1;

    if (rowCount < 2) {
        $j('#wplg_synchronization_btn_process').css("display", "none");
        $j('#wplg-synchronization-table-files-to-import').css("display", "none");
    } else {
        $j('#wplg_synchronization_btn_process').css("display", "block");
        $j('#wplg-synchronization-table-files-to-import').css("display", "block");        
    }
}

function magentoDisplayLoadingMask() {
    var loaderArea = $$('#html-body .wrapper')[0]; // Blocks all page
    Position.clone($(loaderArea), $('loading-mask'), {offsetLeft:-2});
    toggleSelectsUnderBlock($('loading-mask'), false);
    Element.show('loading-mask');
}

/* Export products */
function doExportProducts() {
    try {
        $j.ajax({
            type: "POST",
            data: $j('#wplg_synchronization_export_products_form').serialize(),
            url: $j("#wplg_synchronization_export_products_url").val(),
            beforeSend: function () {
                $j('#wplg_synchronization_export_products_btn').attr("disabled", true).css("opacity", "0.5");
                magentoDisplayLoadingMask();
            },
            success: function (response) {
                console.log(response);
            },
            failure: function () {
                console.log('Ajax request error', 'error');
            },
            complete: function () {
                /* $j('#wplg_synchronization_export_products_btn').attr('disabled', false).css("opacity", "1"); */
                document.location.reload(true);
            }
        });
    } catch (e) {
        console.log(e);
    }
}
/* END */

/* Export categories */
function doExportCategories() {
    try {
        $j.ajax({
            type: "POST",
            data: $j('#wplg_synchronization_export_categories_form').serialize(),
            url: $j("#wplg_synchronization_export_categories_url").val(),
            beforeSend: function () {
                $j('#wplg_synchronization_export_categories_btn').attr("disabled", true).css("opacity", "0.5");
                magentoDisplayLoadingMask();
            },
            success: function (response) {
                console.log(response);
            },
            failure: function () {
                console.log('Ajax request error', 'error');
            },
            complete: function () {
                /* $j('#wplg_synchronization_export_products_btn').attr('disabled', false).css("opacity", "1"); */
                document.location.reload(true);
            }
        });
    } catch (e) {
        console.log(e);
    }
}
/* END */

/* Export product types */
function doExportProductTypes() {
    try {
        $j.ajax({
            type: "POST",
            data: $j('#wplg_synchronization_export_product_types_form').serialize(),
            url: $j("#wplg_synchronization_export_product_types_url").val(),
            beforeSend: function () {
                $j('#wplg_synchronization_export_product_types_btn').attr("disabled", true).css("opacity", "0.5");
                magentoDisplayLoadingMask();
            },
            success: function (response) {
                console.log(response);
            },
            failure: function () {
                console.log('Ajax request error', 'error');
            },
            complete: function () {
                document.location.reload(true);
            }
        });
    } catch (e) {
        console.log(e);
    }
}
/* END */
